<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #DA4453;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Add New Website to Monitor</h4>
            </div>
            <form id="loginForm" method="post" class="form form--login" onsubmit="return add_url()">
                <div class="modal-body">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Add New Url</label>
                        <input type="url" class="form-control" id="url" onblur="return set_tag()" name="url"
                               placeholder="Enter URL"
                               required="required">
                        Please Enter URL as <span style="color:limegreen">http://www.xyz.com</span>. We don't support
                        <span style="color:red">https://</span> in URL.
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Your Website Name</label>
                        <input type="text" class="form-control" name="url_name" id="tag" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="dismiss" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" id="submit" class="btn btn-theme04">ADD</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--modal end here-->
<script>
    function add_url() {
        console.log("ok till now.");
        var url = document.getElementById("url").value;
        var tag = document.getElementById("tag").value;
        $.ajax({
            type: "POST",
            url: 'Add_URL_Handler.php',
            data: {
                "url": url,
                "tag": tag
            },
            success: function (data) {
                var op = JSON.parse(data);
                console.log(data);
                if (op.status == "added") {
                    swal("Website Added Successfully.");
                    document.getElementById("url").value = "";
                    document.getElementById("tag").value = "";
                    update_notification();
                    // location.reload();
                } else if (op.status == "invalid_url") {
                    swal("Invalid URL!");
                } else if (op.status == "failed") {
                    swal("Failed! Check If This Website is Already Added ?");
                } else if (op.status == "missing_www") {
                    swal("URL Must use http://www or https://www .");
                }else {
                    swal("Unknown Status!");
                }

            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    swal("Not connection", "Not connection ,Verify Network.", "error");
                } else if (jqXHR.status == 404) {
                    swal("Not Found", "Requested page not found. [404]", "error");
                } else if (jqXHR.status == 500) {
                    swal("Internal Server Error", "Internal Server Error [500].", "error");
                } else if (exception === 'parsererror') {
                    swal("parse error", "Requested JSON parse failed.", "error");
                } else if (exception === 'timeout') {
                    swal("Request Timeout", "Time out error.", "error");
                } else if (exception === 'abort') {
                    swal("Request Aborted", "Ajax request aborted.", "error");
                } else {
                    swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                }
            }

        });
        return false;
    }
    function set_tag() {
        if (document.getElementById("url").value.split(".").lenght == 2) {
            document.getElementById("tag").value = document.getElementById("url").value.split(".")['0'];
        }
        if (document.getElementById("url").value.split(".").lenght == 3) {
            document.getElementById("tag").value = document.getElementById("url").value.split(".")['1'];
        }

    }
</script>