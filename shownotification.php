<?php
session_start();
include 'connection.php';
include 'function.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="">

    <title>Web Awake</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/Notify.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <!--    <link href="assets/css/table-responsive.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="jquerypage/css/styles.css">
    <link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>
    <script src="jquerypage/js/jquery.quick.pagination.min.js"></script>

    <!--    <script>-->
    <!--        $(document).ready(function() {-->
    <!--            $('#all_logs').DataTable();-->
    <!--        } );-->
    <!--    </script>-->
    <script>
        $(document).ready(function () {
            console.log("STARTING...........");
            $("ul.pagination1").quickPagination();
        });
        $(document).ready(function() {
            $('#notification').DataTable({
                "order": []
            });
        } );
    </script>
</head>

<body>

<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php
    include 'header.php';
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <?php
    include 'sidebar.php';
    ?>
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <!-- Modal -->
    <?php include 'Add_Url.php'; ?>
    <!--modal end here-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-4 col-xs-4">

                </div>
                <div class="col-md-8 col-xs-8">
                    <button class="btn btn-theme04" style="float:right; display:inline-block; margin-top:20px;"
                            data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus"></i>
                        ADD NEW URL
                    </button>
                </div>
            </div>
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="content-panel ds">
                        <h3 id="noti" style="margin-top: -15px;">Notifications</h3>

                        <div class="panel-body ">

                            <ul class="dashboard-list metro pagination1">
                                <?php
                                $query = "SELECT text, DATE_FORMAT(convert_tz(time,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as time FROM notification WHERE user = '" . $_SESSION['email'] . "' ORDER BY id DESC";
                                $result = mysqli_query($conn, $query);
                                if (mysqli_num_rows($result) != 0) {
                                    echo "<table class='table' id='notification'><thead><tr hidden><th>All Notifications</th></tr></thead><tbody>";
                                    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                        ?>
                                      <tr><td>  <li style="padding-bottom: 20px; padding-top: 20px;">

                                            <i class="<?php echo getClass($data['text']); ?>" style="float:left;"></i>

                                            <a href="#"><?php echo $data['text'] . " at " . $data['time'] . "."; ?>
                                            </a>
                                        </li>
                                          </td></tr>

                                        <?php
                                    }
                                    echo "</tbody></table>";
                                } else {
                                    echo "No New Notification";
                                }
                                ?>

                            </ul>
                        </div>
                    </div>
                    <!-- /content-panel -->
                </div>
                <!-- /col-lg-4 -->
            </div>
            <!-- /row -->
        </section>
        <! --/wrapper -->
    </section>
    <! --/wrapper -->
    <!-- /MAIN CONTENT -->

    <!--main content end-->
    <!--footer start-->
    <?php
    include 'footer.php';
    ?>
    <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/js/common-scripts.js"></script>


</body>
</html>
