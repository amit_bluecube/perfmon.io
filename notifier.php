<?php
session_start();
include 'connection.php';
include 'function.php';
include 'email.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);
}
$notification = array();
$query = "select * from notification where user ='" . $_SESSION['email'] . "' AND status='NR';";
//   echo $query;
$result = mysqli_query($conn, $query);
if (mysqli_num_rows($result) != 0) {
    $notification['total'] = mysqli_num_rows($result);
} else {
    $notification['total'] = 0;

}
echo json_encode($notification);
?>