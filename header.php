<header class="header black-bg">
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
    <!--logo start-->
    <a href="dashboard.php" class="logo"><b>Web Awake</b></a>

    <div class="top-menu">
        <ul class="nav pull-right top-menu">
            <!-- inbox dropdown start-->
            <li id="header_inbox_bar" class="dropdown">

                <a data-toggle="dropdown" class="dropdown-toggle" href="">
                    <i class="fa fa-bell"></i>
                    <?php
                    $query = "select * from notification where user ='" . $_SESSION['email'] . "' AND status='NR';";
                    //   echo $query;
                    $result = mysqli_query($conn, $query);
                    ?>
                    <span id="notification_count" class="badge bg-theme"><?php if (mysqli_num_rows($result) != 0) {
                            echo mysqli_num_rows($result);
                        } ?></span>
                </a>

                <ul class="dropdown-menu extended inbox" id="notificationContainer">
                    <div class="notify-arrow notify-arrow-green"></div>

                    <li>
                        <p><span style="float: left">Notifications</span><span style="float: right;"> <button
                                    type="button" id="mark_all_as_read" class="btn btn-theme04 btn-xs">Mark
                                    All as
                                    read
                                </button></span></p>
                    </li>
                    <li>
                        <iframe id="notify" src="http://perfmon.io/notification.php" width="235" height="230"
                                frameborder="0"></iframe>
                    </li>
                   <!-- <li>
                        <p align="center" style="height: 35px;"></p>
                    </li>-->

                </ul>

            </li>
            <!-- inbox dropdown end -->
            <!-- /.dropdown -->
            <li class="dropdown" id="dropdown">
                <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
                    <img src="<?php echo $_SESSION['profile_pic']; ?>" style="height: 22px; width: 22px;"
                         class="img-circle special-img"><?php echo $_SESSION['name']; ?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu extended inbox" id="dropdown-menu_extended">
                    <div class="notify-arrow notify-arrow-green"></div>
                    <li>
                        <p>You have Your Setings Here</p>
                    </li>
                    <li>
                        <a href="profile.php" id="accounts"><i class="fa fa-cog"></i> Account</a>
                    </li>
                    <li data-toggle="modal" data-target="#myModal"><a href="#modal"><i class="fa fa-plus-circle"></i>
                            Add new URL</a></li>
                    <li id="stats"><a href="downhistory.php"><i class="fa fa-bar-chart"></i> Stats</a>
                    <li id="signout"><a href="logout.php"><i class="fa fa-sign-out"></i> Sign-out</a></li>
                </ul>
            </li>
        </ul>
        <!----nav menu end----->
        <script type="text/javascript">
            function update_notification() {
                $.ajax({
                    url: 'notifier.php',
                    success: function (data) {
                        console.log(data);
                        var no_of_notification = JSON.parse(data).total;
                        console.log(no_of_notification);
                        document.getElementById("notification_count").textContent = no_of_notification;
                        document.getElementById("notification_count").style.display = 'block';

                        console.log(document.getElementById("notification_count").textContent);
                        if (parseInt(no_of_notification) > 0) {

                            document.getElementById("notify").contentDocument.location.reload(true);
                        }
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            }
           /* setInterval(function () {
                update_notification() // this will run after every 5 seconds
            }, 100000);*/
            $('#mark_all_as_read').click(function () {

                $.ajax({
                    type: "POST",
                    url: 'mark_notification_as_read.php',
                    data: "id=" + "all",
                    success: function (data) {
                        $("#notify").hide();
                        $("#notify").attr("display", "none");
                        // document.getElementsById("notificationContainer").style.display = "none";
                        // swal(data);// alert the data from the server
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });
            $(document).ready(function () {
                $("#header_inbox_bar").click(function () {
                    $("#notificationContainer").fadeToggle(300);
                    $("#notification_count").fadeOut("slow");

                    return false;

                });

                //Document Click hiding the popup
                $(document).click(function () {
                    $("#notificationContainer").hide();
                });

                //Popup on click
                $("#notificationContainer").click(function () {
                    return false;
                });

            });
        </script>

    </div>
</header>