<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
include 'connection.php';
include 'function.php';
include 'email.php';
$start = microtime(true);
echo "Started at " . $start . "\n";
$query = "select DISTINCT url from user_url";
$result = mysqli_query($conn, $query);
if ($result && mysqli_num_rows($result) != 0) {
    //fetch all urls
    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
        $url = $data['url'];
        $current_status = getWebsiteStatus($url);
        if ($current_status != 200) {
//website is not ok so so get status again
            $current_status = getWebsiteStatus($url);
            if ($current_status != 200) {
// website is not ok so get previous state
                $query1 = "select * from currently_down where url='" . $url . "'";
                $result1 = mysqli_query($conn, $query1);
                if ($result1 && mysqli_num_rows($result1) != 0) {
//website was down and still down so this is the case of DOWN ------> DOWN so get details and update the last checked time(till in currently_down)
                    while ($data1 = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
                        $start_time = $data1['start'];
                        $last_fetched_time = $data1['till'];
                        $last_status = $data1['status'];
                        if ($current_status != $last_status) {
                            //move row from currently_down to down_history
                            //DOWN ------> DOWN_STATUS_CHANGED
                            $query3 = "INSERT INTO down_history (url,start,till,status) select currently_down.url,currently_down.start,currently_down.till,currently_down.status from currently_down where currently_down.url = '" . $url . "';";
                            $result3 = mysqli_query($conn, $query3);
                            if ($result3) {

                            }
                            $query4 = "delete from currently_down where currently_down.url = '" . $url . "';";
                            $result4 = mysqli_query($conn, $query4);
                            if ($result4) {

                            }
                            $query5 = "insert into currently_down (url,start,till,status) values ('" . $url . "',now(),now(),'" . $current_status . "');";
                            $result5 = mysqli_query($conn, $query5);
                            if ($result5) {

                            }
                        } else {
//DOWN ------> DOWN_STATUS_NOT_CHANGED
                            $query6 = "update currently_down set till=now() where url='" . $url . "'";
                            $result6 = mysqli_query($conn, $query6);
                            if ($result6) {

                            }
                        }
                    }
                } else {
                    // UP ---> DOWN
                    //insert into currently_down
                    $query9 = "insert into currently_down (url,start,till,status) values('" . $url . "',now(),now(),'" . $current_status . "');";
                    $result9 = mysqli_query($conn, $query9);
                    if ($result9) {

                    }
                    //NOTIFY VIA MAIL AND WEB NOTIFICATION
                    $query16 = "select user from user_url where url='" . $url . "'";
                    $result16 = mysqli_query($conn, $query16);
                    if ($result16 && mysqli_num_rows($result16) != 0) {
                        while ($data16 = mysqli_fetch_array($result16, MYSQLI_BOTH)) {
                            $email = $data16['user'];
                            $notification = $url . " was detected down";
                            $notified = notify($notification, $email, $conn);
                            if ($notified) {
                                //good
                            } else {
                                error_log($notified);
                            }
                            $email_sent = email_website_down('noreply@perfmon.io', 'perfmon.io', $email, "Admin", $url);
                            if ($email_sent) {
                                //echo("Account of ".$_POST['name']." Created.");
                            } else {
                                // echo($email_sent);
                            }
                        }
                    }
                }

            } else {
                //website is now ok get previous state
                $query8 = "select * from currently_down where url='" . $url . "'";
                $result8 = mysqli_query($conn, $query8);
                if ($result8 && mysqli_num_rows($result8)) {
//DOWN -----> UP
                    //move row from currently_down to down_history
                    $query12 = "INSERT INTO down_history (url,start,till,status) select currently_down.url,currently_down.start,currently_down.till,currently_down.status from currently_down where currently_down.url = '" . $url . "';";
                    $result12 = mysqli_query($conn, $query12);
                    if ($result12) {

                    }
                    $query13 = "delete from currently_down where currently_down.url = '" . $url . "';";
                    $result13 = mysqli_query($conn, $query13);
                    if ($result13) {

                    }
                    //NOTIFY VIA MAIL AND WEB NOTIFICATION
                    $query15 = "select user from user_url where url='" . $url . "'";
                    $result15 = mysqli_query($conn, $query15);
                    if ($result15 && mysqli_num_rows($result15) != 0) {
                        while ($data15 = mysqli_fetch_array($result15, MYSQLI_BOTH)) {
                            $email = $data15['user'];
                            $notification = $url . " has gone up";
                            $notified = notify($notification, $email, $conn);
                            if ($notified) {
                                //good
                            } else {
                                error_log($notified);
                            }
                            $email_sent = email_website_up('noreply@perfmon.io', 'perfmon.io', $email, "Admin", $url);
                            if ($email_sent) {
                                //echo("Account of ".$_POST['name']." Created.");
                            } else {
                                // echo($email_sent);
                            }
                        }
                    }
                } else {
//UP ---------> UP
                    //do nothing
                }
            }
        } else {
            //website is now ok get previous state
            $query7 = "select * from currently_down where url='" . $url . "'";
            $result7 = mysqli_query($conn, $query7);
            if ($result7 && mysqli_num_rows($result7) != 0) {
//DOWN -----> UP
                //move row from currently_down to down_history
                $query10 = "INSERT INTO down_history (url,start,till,status) select currently_down.url,currently_down.start,currently_down.till,currently_down.status from currently_down where currently_down.url = '" . $url . "';";
                $result10 = mysqli_query($conn, $query10);
                if ($result10) {

                }
                $query11 = "delete from currently_down where currently_down.url = '" . $url . "';";
                $result11 = mysqli_query($conn, $query11);
                if ($result11) {

                }
                //NOTIFY VIA MAIL AND WEB NOTIFICATION
                $query14 = "select user from user_url where url='" . $url . "'";
                $result14 = mysqli_query($conn, $query14);
                if ($result14 && mysqli_num_rows($result14) != 0) {
                    while ($data14 = mysqli_fetch_array($result14, MYSQLI_BOTH)) {
                        $email = $data14['user'];
                        $notification = $url . " has gone up";
                        $notified = notify($notification, $email, $conn);
                        if ($notified) {
                            //good
                        } else {
                            error_log($notified);
                        }
                        $email_sent = email_website_up('noreply@perfmon.io', 'perfmon.io', $email, "Admin", $url);
                        if ($email_sent) {
                            //echo("Account of ".$_POST['name']." Created.");
                        } else {
                            // echo($email_sent);
                        }
                    }
                }
                /*   if ($result10 && $result11) {

                   }*/

            } else {
//UP ---------> UP
                //do nothing
            }
        }
    }

} else {
    //do nothing
}
$end = microtime(true);
echo "Completed at " . $end . "\n";
echo "Total execution Time : " . ($end - $start) . "Minute OR " . (($end - $start) / 60) . "\n";

?>