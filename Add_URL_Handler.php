<?php
session_start();
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);
}
include 'connection.php';
include 'function.php';
include 'email.php';
$status = array();
if (isset($_POST['url']) && isset($_POST['tag'])) {
    $url = $_POST['url'];
    /*$url_arr = explode(".", $url);
    if (count($url_arr) < 3) {
        $status['status'] = "invalid_url";
        echo json_encode($status);
        die();
    } else {
        if ($url_arr[0] != "http://www") {
            $status['status'] = "invalid_url";
            echo json_encode($status);
            die();
        }
    }*/
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
        $status['status'] = "invalid_url";
        echo json_encode($status);
        die();
    } else {
        $url_parts=explode(".",$url);
        if(count($url_parts)<3){
            $status['status'] = "missing_www";
            echo json_encode($status);
            die();
        }
       $url_parts[0]="http://www";
       $url=implode(".",$url_parts);

        /*
        $url=str_ireplace("https://www.", "http://www.", $url);
        $url=str_ireplace("www.", "http://www.", $url);
        $url=str_ireplace("http://.", "http://www.", $url);
        $url=str_ireplace("https://.", "http://www.", $url);*/
    }
    // echo $url;
    $tag = $_POST['tag'];
    //  echo $tag;
    $query = "insert into user_url(user,url,tag) values('" . $_SESSION['email'] . "','" . $url . "','" . $tag . "');";
    //echo $query;
    $result = mysqli_query($conn, $query);
    if ($result) {
        //echo "Website Added Successfully.";
        $status['status'] = "added";
        //NOTIFY VIA MAIL AND WEB NOTIFICATION
        $notification = $url . " was successfully added";
        $notified = notify($notification, $_SESSION['email'], $conn);
        if ($notified) {
            //good
        } else {
            error_log($notified);
        }
        $email_sent = email_website_added('noreply@perfmon.io', 'perfmon.io', $_SESSION['email'], $_SESSION['name'], $url);
        if ($email_sent) {
            //echo("Account of ".$_POST['name']." Created.");
        } else {
            // echo($email_sent);
        }
    } else {
        //  echo "Failed! It May be Already added by You.";
        $status['status'] = "failed";
        error_log(mysqli_error($conn));
    }
    echo json_encode($status);
}
?>

