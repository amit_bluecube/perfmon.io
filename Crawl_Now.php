<?php
include 'connection.php';
include 'function.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame 
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Crawl_Now</title>
  <meta name="description" content="">
  <meta name="author" content="Samgan">

  <meta name="viewport" content="width=device-width; initial-scale=1.0">

  <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
</head>

<body>
  
    
     
    <?php
    
    	if (isset($_GET[url])) {
    			
    		$url = $_GET[url];
			
			
			$ch = curl_init($url);
	        curl_setopt($ch, CURLOPT_NOBODY, true);
	        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	        curl_exec($ch);
	        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	        curl_close($ch);
			
		
			$today = getdate();
			$timestamp = $today[month]." ".$today[mday]." ".$today[year]." ".$today[hours].":".$today[minutes].":".$today[seconds];	
			
			if ($retcode==200) {
				
				$query2 = "UPDATE Url_Status SET LastCrawl_Timestamp='$timestamp', Status='OK' WHERE Url='$url'";
				$result2 = mysqli_query($conn, $query2);
				
				
				//site exiting from dump
				$query3 = "UPDATE Dump SET Out_Timestamp='$timestamp', Down_Phase='NILL', Dump_Status='FALSE' WHERE Url='$url'";
				$result3 = mysqli_query($conn, $query3);
				
				
				//calculating duration
				$query4 = "SELECT In_Timestamp, Out_Timestamp FROM Dump WHERE Url='$url'";
				
				$result4 = mysqli_query($conn, $query4);
				$rows4 = mysqli_fetch_assoc($result4);
				$in = explode(" ",$rows4[In_Timestamp]);
				$out = explode(" ",$rows4[Out_Timestamp]);
				
				//calling duration function
				$duration = Cal_Duration($in, $out);
			
				
				$query4 = "UPDATE Dump SET Duration_min = '$duration' WHERE Url='$url'";
				$result4 = mysqli_query($conn, $query4);
				
				$noti = "Your Site \"$url\" Is Up On $timestamp Was Down For $duration mins";
				$query_noti = "INSERT INTO `Notification` VALUES ('$url','$noti', 'UP')";
				$result_noti = mysqli_query($conn, $query_noti);
				
				echo '<script type="text/javascript">';
				echo 'alert("We Are Just Crawling Your Website....");';
				echo '</script>';
				echo '<script type="text/javascript">';
                echo 'window.location.href="' . 'table.php' . '";';
                echo '</script>';
				
				
				
			}else{
				
				$query2 = "UPDATE Url_Status SET LastCrawl_Timestamp='$timestamp' WHERE Url='$url'";
				$result2 = mysqli_query($conn, $query2);
				
				echo '<script type="text/javascript">';
				echo 'alert("We Are Just Crawling Your Website....");';
				echo '</script>';
				echo '<script type="text/javascript">';
                echo 'window.location.href="' . 'table.php' . '";';
                echo '</script>';
				
			}
			
			
		}
    
    ?> 
    
</body>
</html>
