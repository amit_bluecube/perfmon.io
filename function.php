<?php
function isEmail($email)
{
    if (!isset($email)) {
        return false;
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}

function isUrl($url)
{
    if (!isset($url)) {
        return false;
    }
    if (filter_var($url, FILTER_VALIDATE_URL)) {
        return true;
    } else {
        return false;
    }
}

function getWebsiteStatus($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $status;
}

function notify($text, $user_email, $connection)
{
    $query = "insert into notification(text,user) values('" . $text . "','" . $user_email . "');";
    $result = mysqli_query($connection, $query);
    if ($result) {
        return true;
    } else {
        return mysqli_error($connection);
    }
}

function getDownPercentage($url, $connection, $user_email)
{
    $total_down_time = 0;
    $added_ago = 0;
    $query1 = "select TIMESTAMPDIFF(MINUTE,user_url.date,NOW()) as added_ago from user_url where user='" . $user_email . "' and url='" . $url . "'";
    //  echo $query1;
    $result1 = mysqli_query($connection, $query1);
    if (mysqli_num_rows($result1) != 0) {
        while ($data1 = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
            $added_ago = $data1['added_ago'];
        }
        //  echo($total_down_time)."<br>";
        $query3 = "select timestampdiff(minute,start,till) as _diff from currently_down where url='" . $url . "';";
        $result3 = mysqli_query($connection, $query3);
        if (mysqli_num_rows($result3) != 0) {
            while ($data3 = mysqli_fetch_array($result3, MYSQLI_BOTH)) {
                $total_down_time = $total_down_time + $data3['_diff'];
            }
            //  echo($total_down_time)."<br>";
        }
        $query2 = "SELECT down_history.start,down_history.till, down_history.url, TIMESTAMPDIFF(MINUTE , down_history.start, down_history.till ) as down_diff FROM down_history LEFT JOIN user_url ON down_history.start >= user_url.date where down_history.url=user_url.url and down_history.url='" . $url . "' and user_url.user='" . $user_email . "';";
        $result2 = mysqli_query($connection, $query2);
        if (mysqli_num_rows($result2) != 0) {
            while ($data2 = mysqli_fetch_array($result2, MYSQLI_BOTH)) {
                $total_down_time = $total_down_time + $data2['down_diff'];
                //   echo $total_down_time;
            }

            //  echo($total_down_time)."<br>";

        }
    } else {
        //  $total_down_time=100;
    }
    // echo $total_down_time . "|" . ($added_ago-7);
    //Need To Record first curl time
    return ($total_down_time / ($added_ago - 6)) * 100;
}

function getDownPercentageForAllWebsites($connection, $user_email)
{
    $avg_down_time_percentage = 0;
    $total_down_time_percentage = 0;
    $query = "select url from user_url where user='" . $user_email . "'";
    $result = mysqli_query($connection, $query);
    if (mysqli_num_rows($result) != 0) {
        $i = 1;
        while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $total_down_time_percentage = $total_down_time_percentage + getDownPercentage($data['url'], $connection, $user_email);
            $i++;
        }
        $avg_down_time_percentage = $total_down_time_percentage / $i;
    }
    return $avg_down_time_percentage;

}

//has gone up
function getClass($notification)
{
    if (strpos($notification, 'gone up') !== false) {
        return "fa fa-arrow-up green";
    }
    if (strpos($notification, 'detected down') !== false) {
        return "fa fa-arrow-down red";
    }
    if (strpos($notification, 'successfully added') !== false) {
        return "fa fa-plus blue";
    }
    if (strpos($notification, 'successfully removed') !== false) {
        return "fa fa-minus gray";
    }

}

function getTotalDownTime($url, $connection, $user_email)
{
    $total_down_time = 0;
    $added_ago = 0;
    $query1 = "select TIMESTAMPDIFF(MINUTE,user_url.date,NOW()) as added_ago from user_url where user='" . $user_email . "' and url='" . $url . "'";
    //  echo $query1;
    $result1 = mysqli_query($connection, $query1);
    if (mysqli_num_rows($result1) != 0) {
        while ($data1 = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
            $added_ago = $data1['added_ago'];
        }
        //  echo($total_down_time)."<br>";
        $query3 = "select timestampdiff(minute,start,till) as _diff from currently_down where url='" . $url . "';";
        $result3 = mysqli_query($connection, $query3);
        if (mysqli_num_rows($result3) != 0) {
            while ($data3 = mysqli_fetch_array($result3, MYSQLI_BOTH)) {
                $total_down_time = $total_down_time + $data3['_diff'];
            }
            //  echo($total_down_time)."<br>";
        }
        $query2 = "SELECT down_history.start,down_history.till, down_history.url, TIMESTAMPDIFF(MINUTE , down_history.start, down_history.till ) as down_diff FROM down_history LEFT JOIN user_url ON down_history.start >= user_url.date where down_history.url=user_url.url and down_history.url='" . $url . "' and user_url.user='" . $user_email . "';";
        $result2 = mysqli_query($connection, $query2);
        if (mysqli_num_rows($result2) != 0) {
            while ($data2 = mysqli_fetch_array($result2, MYSQLI_BOTH)) {
                $total_down_time = $total_down_time + $data2['down_diff'];
                //   echo $total_down_time;
            }

            //  echo($total_down_time)."<br>";

        }
    } else {
        //  $total_down_time=100;
    }
    // echo $total_down_time . "|" . ($added_ago-7);
    //Need To Record first curl time
    return ($total_down_time / ($added_ago - 6)) * 100;
}

function convertTimeZone($time, $toTimezone, $fromTimeZone)
{
    // timezone by php friendly values
    $date = new DateTime($time, new DateTimeZone($fromTimeZone));
    $date->setTimezone(new DateTimeZone($toTimezone));
    $time = $date->format('Y-m-d h:i:s A');
    return $time;
}

function getOffset($timezone)
{
    $time = new \DateTime('now', new DateTimeZone($timezone));
    $timezoneOffset = $time->format('P');
    return $timezoneOffset;
}

?>

