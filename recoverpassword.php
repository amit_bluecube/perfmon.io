<?php
include 'connection.php';
include 'email.php';
include 'function.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Awake | Recover Password</title>
    <link rel="stylesheet" href="assets/css/style_login.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body id="body_identity" class="align">
<div class="col-sm-offset-4"></div>
<div class="col-xs-8 col-sm-4">
    <?php
    if (!isset($_GET['id']) || !isset($_GET['uid'])) {
        echo "Invalid Request.";
    } else {
        $id = $_GET['id'];
        $email = $_GET['uid'];
        $query = "SELECT email, token,time FROM password_change_requests WHERE email = '" . $email . "' AND (DATE_SUB(now(),INTERVAL 2 HOUR ) < time);";
        //echo $query;
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) != 0) {
            while ($data = mysqli_fetch_array($result, MYSQL_BOTH)) {
                $token = $data['token'];
            }
            if (password_verify($token, $id)) {

                $action = "http://www.perfmon.io/recoverpassword.php?id=" . $id . "&uid=" . $email;
                if (isset($_POST['submit'])) {
                    //     echo "Button Clicked";
                    //echo "RE:- " . $_POST['re_password'];
                    if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['re_password'])) {
                        $email1 = $_POST['email'];
                        $password = $_POST['password'];
                        $re_password = $_POST['re_password'];
                        // echo $email1;
                        if ($password == $re_password) {
                            $query2 = "update users set password='" . password_hash($password, PASSWORD_BCRYPT, ['cost' => 15]) . "' where email='" . $email1 . "'";
                            //      echo $query2;
                            $result2 = mysqli_query($conn, $query2);
                            if ($result2) {
                                //password updated successfully
                                $query3 = "delete from password_change_requests where email='" . $email1 . "'";
                                $result3 = mysqli_query($conn, $query3);
                                if ($result3) { ?>
                                    <h4 align="center" style="color:#4DBF4D;"><i class="fa fa-check"></i>
                                        <?php  echo "Password Updated Successfully"; ?></h4>

                                   <?php
                                    $verified = "YES";
                                } else {
                                    $verified = "YES"; ?>
                                    <h4 align="center" style="color:#4DBF4D;"><i class="fa fa-check"></i>
                                        <?php  echo "Password Changed Successfully , You Can Login Now."; ?></h4>

                                   <!-- echo "Password Changed Successfully , You Can Login Now.";-->
                               <?php }
                            }
                        } else {
                            echo '<p style="color: #D43037; font-size:15px; margin-top: 5px;" align="center">
                                <i class="fa fa-times"></i>
 Password Mismatch. </p>';

                        }
                    } else {
                        //some blank fields
                        //echo "here";
                    }

                } else {

                }
                if ($verified != "YES") {
                    ?>
                    <h2>Reset your password?</h2>

                    <p>We can help you reset your password and security info. Please enter your Email ID</p>
                    <form action="<?php echo $action; ?>" method="post">
                    <div class="form-group">
                        <input type="email" required="required" name="email" id="email" placeholder="Enter your Email"
                               class="form-control">
                        <input type="password" required="required" name="password" id="password"
                               placeholder="New Password" class="form-control">
                        <input type="password" required="required" name="re_password" id="re_password"
                               placeholder="Confirm Password" class="form-control">
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 col-sm-offset-7">
                            <button class="btn btn-theme03" type="submit" name="submit" id="submit"
                                    style="padding:12px 16px 12px 24px; font-weight: bold;">
                                Change Password
                            </button>
                        </div>
                    </div>
                    <?php
                }

                ?>
                </form>
                <?php
            } else {
                //Invalid Token
                echo '<p style="color: #D43037; font-size:15px; margin-top: 5px;" align="center"><i class="fa fa-exclamation-triangle"></i>
                Invalid Token </p>';
            }
        } else {
//Invalid Token
            echo '<p style="color: #D43037; font-size:15px; margin-top: 5px;" align="center"><i class="fa fa-exclamation-triangle"></i>
                Invalid Token </p>';
        }
    }
    ?>
</div>
</body>
</html>