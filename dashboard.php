<?php
session_start();
include 'connection.php';
include 'function.php';
include 'email.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);
}
if ($_COOKIE['is_old_user'] != "yes") {
    error_log("Setting Cookie....");
    setcookie("is_old_user", "yes", time() + 2628000);
    error_log("Setting Cookie Successfull....");
}
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Web Awake</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <link href="assets/css/Notify.css" rel="stylesheet">
    <link href="enjoyhint-master/enjoyhint.css" rel="stylesheet">
    <link href="enjoyhint-master/font_family/jquery.enjoyhint.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/chart-master/Chart.js"></script>
    <script src="highstock/highstock.js"></script>
    <script src="enjoyhint-master/enjoyhint.min.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#currently_down').DataTable();
        });
        $(function () {
            //alert("Hello");
            $('#single_pie').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Average Up-Down Till Now'
                },
                tooltip: {
                    pointFormat: 'All Website: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                <?php
                 $down=getDownPercentageForAllWebsites($conn, $_SESSION['email']);
                 $up=100-$down;
                 if($down>100){
                    $down=100;
                    $up=0;
                 }
                 if($up>100){
                 $up=100;
                 $down=0;
                 }
                 ?>
                series: [{
                    name: "<?php echo $url;?>",
                    colorByPoint: true,
                    data: [{
                        name: "Up",
                        y:<?php echo $up;?>,
                        color: '#46BFBD',

                    }, {
                        name: "Down",
                        y: <?php echo $down ;?>,
                        color: '#F14955',
                        sliced: true,
                        selected: true
                    }]
                }]
            });
        });
    </script>
</head>

<body>

<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!---header start--->
    <?php
    include 'header.php';
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->

    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <p class="centered"><a href="profile.php"><img src="<?php echo $_SESSION['profile_pic']; ?>"
                                                               class="img-circle" width="60"></a>
                </p>
                <h5 class="centered"><?php echo $_SESSION['name']; ?></h5>

                <li class="mt" id="dashboard">
                    <a class="active" href="dashboard.php">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="sub-menu" id="show">
                    <a href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Show all website</span>
                    </a>
                    <ul class="sub">
                        <li id="my-websites"><a href="websites.php"><i class="fa fa-table"></i> My Websites</a></li>
                        <li id="Logs"><a href="downhistory.php"><i class="fa fa-bar-chart"></i> Show Log</a></li>

                    </ul>
                </li>
                <li class="sub-menu" data-toggle="modal" data-target="#myModal" id="new-url">
                    <a href="javascript:;">
                        <i class="fa fa-plus-circle"></i>
                        <span>Add new URL</span>
                    </a>
                </li>
                <li id="report-bug">
                    <a href="report-bug.php">
                        <i class="fa fa-bug"></i>
                        <span>Report Bug</span>
                    </a>
                </li>


            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <!-- Modal -->
    <?php include 'Add_Url.php'; ?>
    <section id="main-content">
        <section class="wrapper">
            <div id="chartjs" class="tab-pane">
                <div class="row-mt">

                    <!--<div class="content-panel">
                       <h4 id="weekly_graph"><i class="fa fa-angle-right"></i> Weekly Report</h4>

                        <div class="panel-body text-center" id="multiple_bar">
                        </div>
                    </div>-->
                    <div class="content-panel table-responsive col-lg-12 ds" style="margin-bottom: 20px;">
                        <h3 style="margin: -15px -15px 10px; font-weight:800;"> Currently DOWN </h3>
                        <section id="unseen">

                            <?php
                            $query = "select currently_down.url as url, DATE_FORMAT(convert_tz(currently_down.start,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as start,DATE_FORMAT(convert_tz(currently_down.till,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as till,timediff(currently_down.till,currently_down.start) as diff,currently_down.status as status,user_url.tag as tag  from currently_down left join user_url on user_url.url=currently_down.url where user_url.user='" . $_SESSION['email'] . "';";

                            //$query = "select currently_down.url as url, currently_down.start as start,currently_down.till as till,timediff(currently_down.till,currently_down.start) as diff,currently_down.status as status,user_url.tag as tag  from currently_down left join user_url on user_url.url=currently_down.url where user_url.user='" . $_SESSION['email'] . "';";
                            //echo $query;
                            $result = mysqli_query($conn, $query);
                            if (mysqli_num_rows($result) != 0) {
                                ?>

                                <table class="table table-bordered table-striped table-condensed" id="currently_down">
                                    <thead>
                                    <tr>
                                        <th>S. No.</th>
                                        <th>Website Name</th>
                                        <th>Website URL</th>
                                        <th>Start</th>
                                        <th>Till</th>
                                        <th>Down Time Duration</th>
                                        <th>Reason</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 1;
                                    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                        echo "<tr><td>" . $i . "</td><td>" . $data['tag'] . "</td><td>" . $data['url'] . "</td><td>" . $data['start'] . "</td><td>" . $data['till'] . "</td><td>" . $data['diff'] . "</td><td>" . $data['status'] . "</td></tr>";
                                        $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <?php
                            } else { ?>
                                <h4 align="center" style="color:#4DBF4D;">Bingo You have No Site Down</h4>
                            <?php }
                            ?>

                        </section>
                    </div>

                </div>
                <div class="row mt">
                    <div class="col-md-6">
                        <div class="content-panel ds">
                            <h3 style="margin-top: -15px; background-color:#46BFBD;"> Up Down Record(%)</h3>

                            <div class="panel-body text-center" id="single_pie">

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="content-panel ds">
                            <h3 id="noti" style="margin-top: -15px; background-color:#fcb322;"> Latest
                                Notifications</h3>

                            <div class="panel-body ">

                                <ul class="dashboard-list metro">
                                    <?php
                                    $query = "SELECT text,DATE_FORMAT(convert_tz(notification.time,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as time FROM notification WHERE user = '" . $_SESSION['email'] . "' ORDER BY id DESC LIMIT 0 , 9";
                                    $result = mysqli_query($conn, $query);
                                    if (mysqli_num_rows($result) != 0) {
                                        while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                            ?>
                                            <li style="padding-bottom: 20px; padding-top: 20px;">

                                                <i class="<?php echo getClass($data['text']); ?>"
                                                   style="float:left;"></i>

                                                <a href="#"><?php echo $data['text'] . " at " . $data['time'] . "."; ?>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                    } else {
                                        echo "No New Notification";
                                    }
                                    ?>

                                    <li style="padding-bottom: 20px; padding-top: 20px;text-align: right;">
                                        <a href="shownotification.php">

                                            See more <i class="fa fa-arrow-right "></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>


                <!--weekly notification ends here-->
            </div>
            <!--main content end-->
        </section>
    </section>
    <!--footer start-->
    <?php
    include 'footer.php';
    ?>
    <!--footer end-->
</section>
<!---enjoy hint script--->
<!--<script>
    //initialize instance
    var enjoyhint_instance = new EnjoyHint({});

    //simple config.
    //Only one step - highlighting(with description) "New" button
    //hide EnjoyHint after a click on the button.
    var enjoyhint_script_steps = [

        {
            'next #dashboard' : 'This is your dashboard'
        },
        {
            'click #show' : 'This is a paragraph'
        }

    ];
    var options = {
        "next #block": 'Hello.',
        "nextButton" : {className: "myNext", text: "NEXT"},
        "skipButton" : {className: "mySkip", text: "SKIP"},

    }


    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);

    //run Enjoyhint script
    enjoyhint_instance.run();
</script>-->
<!--end enjoyhint script-->

<!-- js placed at the end of the document so the pages load faster -->
<!--    <script src="assets/js/jquery.js"></script>-->
<!--    <script src="assets/js/jquery-1.8.3.min.js"></script>-->
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/js/jquery.sparkline.js"></script>
<script src="assets/js/common-scripts.js"></script>
</body>
</html>
