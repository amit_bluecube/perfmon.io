<?php
session_start();
include 'connection.php';
include 'function.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: sign_in.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'sign_in.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'sign_in.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="">

    <title>Web Awake</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <link href="assets/css/table-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#all_logs').DataTable();
        } );
    </script>
</head>

<body>

<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <?php
    include 'header.php';
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <p class="centered"><a href="profile.php"><img src="<?php echo $_SESSION['profile_pic'] ?>"
                                                               class="img-circle" width="60"></a></p>
                <h5 class="centered"><?php echo $_SESSION['name'] ?></h5>

                <li class="mt">
                    <a href="dashboard.php">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a class="active" href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Show all Website</span>
                    </a>
                    <ul class="sub">
                        <li><a href="websites.php"><i class="fa fa-table"></i> My Websites</a></li>
                        <li class="active"><a href="downhistory.php"><i class="fa fa-bar-chart"></i> Show Log</a></li>
                    </ul>
                </li>
                <li class="sub-menu" data-toggle="modal" data-target="#myModal">
                    <a href="javascript:;">
                        <i class="fa fa-plus-circle"></i>
                        <span>Add new URL</span>
                    </a>
                </li>
                <li id="report-bug">
                    <a href="report-bug.php">
                        <i class="fa fa-bug"></i>
                        <span>Report Bug</span>
                    </a>
                </li>

            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <!-- Modal -->
    <?php include 'Add_Url.php'; ?>
    <!--modal end here-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-4 col-xs-4">

                </div>
                <div class="col-md-8 col-xs-8">
                    <button class="btn btn-theme04" style="float:right; display:inline-block; margin-top:20px;"
                            data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus"></i>
                        ADD NEW URL
                    </button>
                </div>
            </div>
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="content-panel table-responsive ds">
                        <h3 style="margin-bottom: 10px; margin-top: -15px;"> My Logs</h3>
                        <section id="unseen"style="margin-left: 10px; margin-right: 10px;">

                            <?php
                            //DATE_FORMAT(NOW(),'%e %b %Y %h:%i %s %p')
                            $query = "SELECT down_history.url as url, DATE_FORMAT(convert_tz(down_history.start,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as start, DATE_FORMAT(convert_tz(down_history.till,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as till,TIMESTAMPDIFF(MINUTE,down_history.start,down_history.till) as duration, down_history.status as status,user_url.tag as tag FROM down_history LEFT JOIN user_url ON user_url.url = down_history.url WHERE user_url.user = '" . $_SESSION['email'] . "'";
                            //echo $query;
                            $result = mysqli_query($conn, $query);
                            if (mysqli_num_rows($result) != 0) {
                                ?>
                                <table class="table table-bordered table-striped table-condensed" id="all_logs">
                                    <thead>
                                    <tr>
                                        <th>S. No.</th>
                                        <th>Website Name</th>
                                        <th>Website URL</th>
                                        <th>Start</th>
                                        <th>Till</th>
                                        <th>Duration</th>
                                        <th>Status</th>
                                        <th>Help Me</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 1;
                                    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {

                                        echo "<tr><td>" . $i . "</td><td><a target=\"_blank\" href=\"" . $data['url'] . "\">".$data['tag']."</a></td><td><a target=\"_blank\" href=\"" . $data['url'] . "\">".$data['url']."</a></td><td>" . $data['start'] . "</td><td>" . $data['till']. "</td><td>" . $data['duration'] . " Min</td><td>" . $data['status'] . "</td><td><a href=\"https://en.wikipedia.org/wiki/List_of_HTTP_status_codes\" target=\"_blank\" class=\"btn btn-warning\" >Seek</a></td></tr>";
                                        $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <?php
                            } else {
                                echo "No data found.";
                            }
                            ?>

                        </section>
                    </div>
                    <!-- /content-panel -->
                </div>
                <!-- /col-lg-4 -->
            </div>
            <!-- /row -->
        </section>
        <! --/wrapper -->
    </section>
    <! --/wrapper -->
    <!-- /MAIN CONTENT -->

    <!--main content end-->
    <!--footer start-->
    <?php
    include 'footer.php';
    ?>
    <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<!--<script src="assets/js/jquery.js"></script>-->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


<!--common script for all pages-->
<script src="assets/js/common-scripts.js"></script>

<!--script for this page-->

<!--<script>
    //custom select box

    $(function(){
        $('select.styled').customSelect();
    });

</script>
-->
</body>
</html>
