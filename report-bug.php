<?php
session_start();
include 'connection.php';
include 'email.php';
include 'function.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Web Awake</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>


    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <link href="assets/css/Notify.css" rel="stylesheet">
    <link href="enjoyhint-master/enjoyhint.css" rel="stylesheet">
    <link href="enjoyhint-master/font_family/jquery.enjoyhint.css" rel="stylesheet">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="enjoyhint-master/enjoyhint.min.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>

</head>

<body>

<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!-- Modal -->
    <?php include 'Add_Url.php'; ?>
    <!--modal end here-->
    <!--header start-->
    <?php
    include 'header.php';
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <?php
    include 'sidebar.php';
    ?>
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">

            <div class="row mt">
                <div class="col-lg-10 col-lg-offset-1 ">
                    <div class="form-panel ds">

                        <h3 style="margin: -10px -11px 10px;">Report Your Issue</h3>

                        <form action="report-bug.php" class="form-horizontal style-form" method="post">
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Email Id</label>

                                <div class="col-lg-10">
                                    <p class="form-control-static"><?php echo $_SESSION['email']; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Subject</label>

                                <div class="col-sm-10">
                                    <input type="text" maxlength="150" id="subject" name="subject" class="form-control"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 col-sm-2 control-label">Your Issue</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" maxlength="500" id="issue" name="issue"
                                              required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-theme04" type="submit" name="submit" id="submit"
                                        style="float:right; margin-right:5%;">
                                    Report Bug
                                </button>
                            </div>
                            <?php
                            if (isset($_POST['submit'])) {
                                $subject = trim($_POST['subject']);
                                $issue = trim($_POST['issue']);
                                $email = $_SESSION['email'];
                                $name = $_SESSION['name'];
                                if (strlen($subject) != 0 && strlen($issue) != 0) {
                                    $query = "insert into feedback (email,subject,issue) values('" . $email . "','" . $subject . "','" . $issue . "')";
//                                   echo $query;
                                    $result = mysqli_query($conn, $query);
                                    if ($result) {
                                        echo "<p style='color: #4DBF4D; font-weight:bold; text-align:center;'>Thank you " . $name . " For your valuable feedback . We will ping you for more informaion if required.</p>";
                                    } else {
                                        echo "<p style='color: #F14955;font-weight:bold; text-align:center;'>Thank you " . $name . " For your valuable feedback But we are unable to save your feedback ,please try after sometime.If problem persists you can write us at help@bcntestdomain.com.</p>";

                                    }
                                }
                            }
                            ?>
                        </form>


                    </div>

                </div>
                <!-- col-lg-12-->

            </div>
            <!-- /row -->

        </section>

    </section>
    <!-- /MAIN CONTENT -->


    <!--main content end-->
    <!--footer start-->
    <?php
    include 'footer.php';
    ?>
    <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--common script for all pages-->
<script src="assets/js/common-scripts.js"></script>


<!--script for this page-->

</body>
</html>
