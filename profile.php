<?php
session_start();
include 'connection.php';
include 'email.php';
include 'function.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Web Awake</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <link href="assets/css/Notify.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]--->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <!-- <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>-->


    <script type="text/javascript">
        /* $(function() {
         $("#uploadFile").on("change", function()
         {
         var files = !!this.files ? this.files : [];
         if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

         if (/^image/.test( files[0].type)){ // only image file
         var reader = new FileReader(); // instance of the FileReader
         reader.readAsDataURL(files[0]); // read the local file

         reader.onloadend = function(){ // set image data as background of div
         $("#imagePreview").css("background-image", "url("+this.result+")");
         }
         }
         }); */
        $(document).ready(function () {
            console.log("ready!");
            //update name
            $('#update_name').click(function () {
                // swal("Update Name Clicked...");
                var new_name = document.getElementById("name").value;
                if (new_name.trim().length == 0) {
                    swal("Name Field Empty!", "Name Field Must Be Filled.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "new_name=" + new_name,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("name").value = "";
                        document.getElementById("current_name").innerHTML = new_name;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
                document.getElementById("name").value = "";
            });
            $('#update_password').click(function () {
                // swal("Update Name Clicked...");
                var old_password = document.getElementById("old_pass").value;
                var new_password = document.getElementById("new_pass").value;
                var re_new_password = document.getElementById("confirm_pass").value;
                if (new_password.trim().length == 0 || re_new_password.trim().length == 0) {
                    swal("Password Field Empty!", "Both Password In New Password Field Must Be Filled & Same.", "error");
                    return;
                }
                if (new_password !== re_new_password) {
                    swal("Password Mismatched!", "Both Password In New Password Fields Must Be Same.", "error");
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: {
                        "old_password": old_password,
                        "new_password": new_password,
                        "re_new_password": re_new_password
                    },
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("old_pass").value = "";
                        document.getElementById("new_pass").value = "";
                        document.getElementById("confirm_pass").value = "";
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }
                });


            });
            //change email
            $('#update_email').click(function () {
                // swal("Update Name Clicked...");
                var email = document.getElementById("email").value;
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;

                if (email.trim().length == 0) {
                    swal("Email Field Empty!", "Email Field Must Be Filled.", "error");
                    return;
                }
                if (!re.test(email)) {
                    swal("Invalid Email!", "Email Must Be Valid.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "email=" + email,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("email").value = "";
                        document.getElementById("current_email").innerHTML = email;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });
            //change alternate email
            $('#update_alt_email').click(function () {
                // swal("Update Name Clicked...");
                var alt_email = document.getElementById("alt_email").value;
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
                if (alt_email.trim().length == 0) {
                    swal("Email Field Empty!", "Email Field Must Be Filled.", "error");
                    return;
                }
                if (!re.test(alt_email)) {
                    swal("Invalid Email!", "Email Must Be Valid.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "alt_email=" + alt_email,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("alt_email").value = "";
                        console.log(alt_email);
                        document.getElementById("current_alt_email").innerHTML = alt_email;
                        console.log(document.getElementById("current_alt_email").innerHTML);
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });

            //change contact number
            $('#update_contact_no').click(function () {
                //	 swal("Update Name Clicked...");
                var contact_no = document.getElementById("contact_no").value;
                if (contact_no.trim().length == 0) {
                    swal("Contact Number Field Empty!", "Contact Number Field Must Be Filled.", "error");
                    return;
                }
                if (contact_no % 1 != 0 || contact_no.length != 10 || contact_no < 1) {
                    swal("Invalid Contact Number!", "Contact Number Field Must Be Valid.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "contact_no=" + contact_no,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("contact_no").value = "";
                        document.getElementById("current_contact_no").innerHTML = contact_no;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });
            //change alternate contact number
            $('#update_alt_contact_no').click(function () {
                var alt_contact_no = document.getElementById("alt_contact_no").value;
                // swal(alt_contact_no);
                if (alt_contact_no.trim().length == 0) {
                    swal("Altername Contact Number Field Empty!", "Alternate Contact Number Field Must Be Filled.", "error");
                    return;
                }
                if (alt_contact_no % 1 != 0 || alt_contact_no.length != 10 || alt_contact_no < 1) {
                    swal("Invalid Contact Number!", "Contact Number Field Must Be Valid.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "alt_contact_no=" + alt_contact_no,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("alt_contact_no").value = "";
                        document.getElementById("current_alt_contact_no").innerHTML = alt_contact_no;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });

            //change company name
            $('#update_company_name').click(function () {
                var company_name = document.getElementById("company_name").value;
                // swal(alt_contact_no);
                if (company_name.trim().length == 0) {
                    swal("Empty Company Name!", "Company Name Must Be Filled.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "company_name=" + company_name,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("company_name").value = "";
                        document.getElementById("current_company_name").innerHTML = company_name;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });


            //change job profile
            $('#update_job_profile').click(function () {
                var job_profile = document.getElementById("job_profile").value;
                // swal(alt_contact_no);
                if (job_profile.trim().length == 0) {
                    swal("Empty Job Profile!", "Job Profile Must Be Filled.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "job_profile=" + job_profile,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("job_profile").value = "";
                        document.getElementById("current_job_profile").innerHTML = job_profile;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });
            $('#update_timezone').click(function () {
                var element = document.getElementById("timezone");
                var timezone = element.options[element.selectedIndex].value;
                if (timezone.trim().length == 0) {
                    swal("Empty TimeZone!", "TimeZone Must Be Filled.", "error");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: 'update_user_details.php',
                    data: "timezone=" + timezone,
                    success: function (data) {
                        swal(data);// alert the data from the server
                        document.getElementById("job_profile").value = "";
                        document.getElementById("current_timezone").innerHTML = timezone;
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            swal("Not connection", "Not connection ,Verify Network.", "error");
                        } else if (jqXHR.status == 404) {
                            swal("Not Found", "Requested page not found. [404]", "error");
                        } else if (jqXHR.status == 500) {
                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                        } else if (exception === 'parsererror') {
                            swal("parse error", "Requested JSON parse failed.", "error");
                        } else if (exception === 'timeout') {
                            swal("Request Timeout", "Time out error.", "error");
                        } else if (exception === 'abort') {
                            swal("Request Aborted", "Ajax request aborted.", "error");
                        } else {
                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                        }
                    }

                });
            });


            //change profile pic
            $('#update_profile_pic').on('click', function () {
                var file_data = $('#profile_pic').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                console.log(form_data);
                $.ajax({
                    url: 'change_profile_pic.php', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (php_script_response) {
                        swal(php_script_response); // display response from the PHP script, if any
                    }
                });
            });
        });
    </script>
    <style>
        #imagePreview {
            width: 180px;
            height: 185px;
            background-position: center center;
            background-size: cover;
            -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
            display: inline-block;
            border: thick solid darkgrey;
            background-color: #F0F0F0;
        }
    </style>
</head>

<body>

<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!-- Modal -->
    <?php include 'Add_Url.php'; ?>
    <!--modal end here-->
    <!--header start-->
    <?php
    include 'header.php';
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <?php
    include 'sidebar.php';
    ?>
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <?php
    $company_name = "N/A";
    $job_profile = "N/A";
    $account_created_on = "N/A";
    $last_modification_date = "N/A";
    $secondary_email = "N/A";
    $phone = "N/A";
    $alternate_phone = "N/A";
  //  $query = "select company_name,job_profile,account_created_on,last_modification_date,secondary_email,alternate_phone,phone from user_details where email='" . $_SESSION['email'] . "';";
    $query = "select company_name,job_profile,DATE_FORMAT(convert_tz(user_details.account_created_on,'" . getOffset('America/Detroit') . "','".getOffset($_SESSION['timezone']). "'),'%e %b %Y %h:%i %s %p') as account_created_on,DATE_FORMAT(convert_tz(user_details.last_modification_date,'" . getOffset('America/Detroit') . "','".getOffset($_SESSION['timezone']). "'),'%e %b %Y %h:%i %s %p') as last_modification_date,secondary_email,alternate_phone,phone from user_details where email='" . $_SESSION['email'] . "';";
   //echo $query;
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) != 0) {
        while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            if ($data['company_name'] != null) {
                $company_name = $data['company_name'];
            }
            if ($data['job_profile'] != null) {
                $job_profile = $data['job_profile'];
            }
            if ($data['account_created_on'] != null) {
                $account_created_on = $data['account_created_on'];
            }
            if ($data['last_modification_date'] != null) {
                $last_modification_date = $data['last_modification_date'];
            }
            if ($data['secondary_email'] != null) {
                $secondary_email = $data['secondary_email'];
            }
            if ($data['alternate_phone'] != null) {
                $alternate_phone = $data['alternate_phone'];
            }
            if ($data['phone'] != null) {
                $phone = $data['phone'];
            }

        }
    }
    ?>
    <section id="main-content">
        <section class="wrapper site-min-height">

            <div class="row mt">
                <div class="col-lg-12">
                    <div class="form-panel ds">

                        <h3 style="margin: -10px -11px 10px;">Account Settings</h3>

                        <form id="account" class="form-horizontal style-form" method="get">
                            <div class="row mt">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Name</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_name"><?php echo $_SESSION['name']; ?></p>

                                            <div id="demo" class="collapse out">
                                                <div class="col-sm-5" id="data">
                                                    <input type="text" id="name" class="form-control form-size"
                                                           placeholder="New Name">
                                                    <button type="button" id="update_name"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse" data-target="#demo"><i
                                                    class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Password</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static">Choose Strong Password.</p>

                                            <div id="demo1" class="collapse out">
                                                <div class="col-sm-6" id="data">
                                                    <input type="password" id="old_pass" class="form-control form-size"
                                                           placeholder="Old Password">
                                                    <input type="password" id="new_pass" class="form-control form-size"
                                                           placeholder="New Password">
                                                    <input type="password" id="confirm_pass"
                                                           class="form-control form-size"
                                                           placeholder="Confirm Password">
                                                    <button type="button" id="update_password"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo1"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Email</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_email"><?php echo $_SESSION['email']; ?></p>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Alt Email</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_alt_email"><?php echo $secondary_email; ?></p>

                                            <div id="demo2" class="collapse out">
                                                <div class="col-sm-6" id="data">
                                                    <input type="email" id="alt_email" class="form-control form-size"
                                                           placeholder="New Alt Email">
                                                    <button type="button" id="update_alt_email"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo2"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Contact No</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_contact_no"><?php echo $phone; ?></p>

                                            <div id="demo3" class="collapse out">
                                                <div class="col-sm-6" id="data">
                                                    <input type="tel" pattern="[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}"
                                                           id="contact_no" class="form-control form-size"
                                                           placeholder="New contact">
                                                    <button type="button" id="update_contact_no"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo3"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Alt Contact No</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_alt_contact_no"><?php echo $alternate_phone; ?></p>

                                            <div id="demo4" class="collapse out">
                                                <div class="col-sm-6" id="data">
                                                    <input type="text" id="alt_contact_no"
                                                           class="form-control form-size" placeholder="New contact">
                                                    <button type="button" id="update_alt_contact_no"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo4"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Comapny Name</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_company_name"><?php echo $company_name; ?></p>

                                            <div id="demo5" class="collapse out">
                                                <div class="col-sm-5" id="data">
                                                    <input type="text" id="company_name" class="form-control form-size"
                                                           placeholder="New Profile">
                                                    <button type="button" id="update_company_name"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo5"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Job Profile</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_job_profile"><?php echo $job_profile; ?></p>

                                            <div id="demo6" class="collapse out">
                                                <div class="col-sm-5" id="data">
                                                    <input type="text" id="job_profile" class="form-control form-size"
                                                           placeholder="New Profile">
                                                    <button type="button" id="update_job_profile"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo6"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>
                                    </div>
                                </div>
                                <!--                                        <div class="col-md-offset-1"></div>-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Upload Profile Pic</label>

                                        <div class="col-lg-6">
                                            <div id="imagePreview"><img src="<?php echo $_SESSION['profile_pic']; ?>"
                                                                        alt="profile_pic"
                                                                        style="width: 170px; height: 175px;"></div>
                                            <input type="file" id="profile_pic" name="profile_pic" accept="image/*"
                                                   class="img"/>
                                            <button type="button" id="update_profile_pic" name="update_profile_pic"
                                                    class="btn btn-theme03 form-size2">Upload
                                            </button>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Acc Created on</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"><?php echo $account_created_on; ?> </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 col-sm-2 control-label">Time Zone</label>

                                        <div class="col-lg-6" id="data">
                                            <p class="form-control-static"
                                               id="current_timezone"><?php echo $_SESSION['timezone']; ?></p>

                                            <div id="demo7" class="collapse out">
                                                <div class="col-sm-5" id="data">
                                                    <select class="form-control form-size" style="margin-bottom:20px;"
                                                            id="timezone" name="timezone"></select>
                                                    <button type="button" id="update_timezone"
                                                            class="btn btn-theme03 form-size2">Update
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--												<div class="col-lg-6" id="data">-->
                                        <!--													<select class="form-control" style="margin-bottom:20px;" id="timezone" name="timezone"></select>-->
                                        <!---->
                                        <!--												</div>-->
                                        <div class="col-lg-2" id="edit">
                                            <a href="#" class=" form-size" data-toggle="collapse"
                                               data-target="#demo7"><i class="fa fa-pencil"></i>
                                                Edit</a>
                                        </div>


                                    </div>


                                </div>
                            </div>
                            <p style="text-align: right">Last Modified On: <?php echo $last_modification_date; ?> </p>
                        </form>


                    </div>

                </div>
                <!-- col-lg-12-->

            </div>
            <!-- /row -->

        </section>
        <! --/wrapper -->
    </section>
    <!-- /MAIN CONTENT -->


    <!--main content end-->
    <!--footer start-->
    <?php
    include 'footer.php';
    ?>
    <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>

<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--common script for all pages-->
<script src="assets/js/common-scripts.js"></script>
<script src="assets/timezone/timezones.full.min.js"></script>
<script>
    $('select').timezones();
</script>

<!--script for this page-->

</body>
</html>
