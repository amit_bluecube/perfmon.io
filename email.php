<?php
require_once "PHPMailerAutoload.php";
/**
 * @param $sender_email , Your Email.
 * @param $sender_name ,Your Name
 * @param $recipient_email , Receiver's Email
 * @param $recipient_name ,Receiver's Email
 * @param $phase_no , Error Phase No
 * @param $website_url , Receiver's Website URL
 * @param $duration , Down Duration
 * @param $reason , Reason Of Down
 */


function email_account_created($sender_email, $sender_name, $recipient_email, $recipient_name)
{
    $mail = new PHPMailer;
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addAddress($recipient_email, $recipient_name);
    $mail->addReplyTo($sender_email, $sender_name);
    $mail->isHTML(true);
    $mail->Subject = "Account Created";
    $mail->Body = "<div style=\"width: auto;text-align: justify;background-color: bisque;margin-left: 5px;margin-right: 5px;border-radius: 3px\">
Hello " . $recipient_name . ", <br>This is to Inform You that your account is created successfully at " . date("Y/m/d h:i:s a") . " <div>";

    $mail->AltBody = "Hello " . $recipient_name . ",This was to inform you that your account is created successfully at " . date("Y/m/d h:i:s a") . ".";

    if (!$mail->send()) {
        return $mail->ErrorInfo;
    } else {
        return true;
    }

}

function email_website_up($sender_email, $sender_name, $recipient_email, $recipient_name, $url)
{
    $mail = new PHPMailer;
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addAddress($recipient_email, $recipient_name);
    $mail->addReplyTo($sender_email, $sender_name);
    $mail->isHTML(true);
    $mail->Subject = $url . " is Up.";
    $mail->Body = "<div style=\"width: auto;text-align: justify;background-color: bisque;margin-left: 5px;margin-right: 5px;border-radius: 3px\">
Hello " . $recipient_name . ", <br>This is to Inform You that " . $url . " is detected up again at " . date("Y/m/d h:i:s a") . " .<div>";

    $mail->AltBody = "Hello " . $recipient_name . ",This is to inform you that " . $url . " is detected up again at " . date("Y/m/d h:i:s a") . ".";

    if (!$mail->send()) {
        return $mail->ErrorInfo;
    } else {
        return true;
    }

}

function email_website_down($sender_email, $sender_name, $recipient_email, $recipient_name, $url)
{
    $mail = new PHPMailer;
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addAddress($recipient_email, $recipient_name);
    $mail->addReplyTo($sender_email, $sender_name);
    $mail->isHTML(true);
    $mail->Subject = $url . " is Down.";
    $mail->Body = "<div style=\"width: auto;text-align: justify;background-color: bisque;margin-left: 5px;margin-right: 5px;border-radius: 3px\">
Hello " . $recipient_name . ", <br>This is to Inform You that " . $url . " is detected down at " . date("Y/m/d h:i:s a") . ". <div>";

    $mail->AltBody = "Hello " . $recipient_name . ",This is to inform you that " . $url . " is detected down at " . date("Y/m/d h:i:s a") . ".";

    if (!$mail->send()) {
        return $mail->ErrorInfo;
    } else {
        return true;
    }

}

function email_website_added($sender_email, $sender_name, $recipient_email, $recipient_name, $url)
{
    $mail = new PHPMailer;
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addAddress($recipient_email, $recipient_name);
    $mail->addReplyTo($sender_email, $sender_name);
    $mail->isHTML(true);
    $mail->Subject = $url . " is Added.";
    $mail->Body = "<div style=\"width: auto;text-align: justify;background-color: bisque;margin-left: 5px;margin-right: 5px;border-radius: 3px\">
Hello " . $recipient_name . ", <br>This is to Inform You that " . $url . " is added to your list. <div>";

    $mail->AltBody = "Hello " . $recipient_name . ",This is to inform you that " . $url . " is added to your list. ";

    if (!$mail->send()) {
        return $mail->ErrorInfo;
    } else {
        return true;
    }

}

function email_website_removed($sender_email, $sender_name, $recipient_email, $recipient_name, $url)
{
    $mail = new PHPMailer;
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addAddress($recipient_email, $recipient_name);
    $mail->addReplyTo($sender_email, $sender_name);
    $mail->isHTML(true);
    $mail->Subject = $url . " is Removed.";
    $mail->Body = "<div style=\"width: auto;text-align: justify;background-color: bisque;margin-left: 5px;margin-right: 5px;border-radius: 3px\">
Hello " . $recipient_name . ", <br>This is to Inform You that " . $url . " is removed from your list. <div>";

    $mail->AltBody = "Hello " . $recipient_name . ",This is to inform you that " . $url . " is removed from your list. ";

    if (!$mail->send()) {
        return $mail->ErrorInfo;
    } else {
        return true;
    }

}

function sendPasswordChangeLink($sender_email, $sender_name, $recipient_email, $recipient_name, $url)
{
    $mail = new PHPMailer;
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addAddress($recipient_email, $recipient_name);
    $mail->addReplyTo($sender_email, $sender_name);
    $mail->isHTML(true);
    $mail->Subject = "Password Change Request.";
    $mail->Body = "<div style=\"width: auto;text-align: justify;background-color: bisque;margin-left: 5px;margin-right: 5px;border-radius: 3px\">
Hello " . $recipient_name . ", <br>This is to Inform You that a password change request is generated for your account ,  If this is not by you then we are afraid that
your account is compromised.If it is you then just goto " . $url . " and reset your password.This link is valid for two hour only.<div>";

    $mail->AltBody = "Hello " . $recipient_name . ",To reset your password goto " . $url . " . This link is valid for two hour only.";

    if (!$mail->send()) {
        return $mail->ErrorInfo;
    } else {
        return true;
    }

}

?>