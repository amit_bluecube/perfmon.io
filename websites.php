<?php
ini_set('display_errors', '1');
session_start();
include 'connection.php';
include 'function.php';

if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);
} ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Web Awake</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">

    <link href="assets/css/table-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <script>
        $(document).ready(function () {
            $('#all_website').DataTable();
        });
        function delete_url(id1, id) {
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this change!",
                    type: "warning",
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete It!",
                    closeOnConfirm: false
                },
                function () {
                    console.log("id= " + id);
                    console.log("id1= " + id1);
                    $.ajax({
                        type: "POST",
                        url: 'delete_url.php',
                        data: "url=" + id,
                        success: function (data) {
                            swal(data);
                            var i_d = '#' + id1;
                            console.log(i_d);
                            $(i_d).parent().hide();
                            if (data == "URL Deleted.") {
//                                document.getElementById(id).style.visibility = "hidden";
//                                document.getElementById(id).visibility = "hidden";
                            }
                        },
                        error: function (jqXHR, exception) {
                            if (jqXHR.status === 0) {
                                swal("Not connection", "Not connection ,Verify Network.", "error");
                            } else if (jqXHR.status == 404) {
                                swal("Not Found", "Requested page not found. [404]", "error");
                            } else if (jqXHR.status == 500) {
                                swal("Internal Server Error", "Internal Server Error [500].", "error");
                            } else if (exception === 'parsererror') {
                                swal("parse error", "Requested JSON parse failed.", "error");
                            } else if (exception === 'timeout') {
                                swal("Request Timeout", "Time out error.", "error");
                            } else if (exception === 'abort') {
                                swal("Request Aborted", "Ajax request aborted.", "error");
                            } else {
                                swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                            }
                        }

                    });
                });

        }


    </script>

</head>

<body>
<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!-- Modal -->
    <?php include 'Add_Url.php'; ?>
    <?php include 'web_edit.php'; ?>
    <!--modal end here-->
    <!--header start-->
    <?php
    include 'header.php';
    ?>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">

                <p class="centered"><a href="profile.php"><img src="<?php echo $_SESSION['profile_pic']; ?>"
                                                               class="img-circle" width="60"></a></p>
                <h5 class="centered"><?php echo $_SESSION['name'] ?></h5>

                <li class="mt">
                    <a href="dashboard.php">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a class="active" href="javascript:;">
                        <i class="fa fa-th"></i>
                        <span>Show all Website</span>
                    </a>

                    <ul class="sub">
                        <li class="active"><a href="websites.php"><i class="fa fa-table"></i> My Websites</a></li>
                        <li><a href="downhistory.php"><i class="fa fa-bar-chart"></i> Show Log</a></li>
                    </ul>
                </li>
                <li class="sub-menu" data-toggle="modal" data-target="#myModal">
                    <a href="javascript:;">
                        <i class="fa fa-plus-circle"></i>
                        <span>Add new URL</span>
                    </a>
                </li>
                <li id="report-bug">
                    <a href="report-bug.php">
                        <i class="fa fa-bug"></i>
                        <span>Report Bug</span>
                    </a>
                </li>

            </ul>
        </div>
    </aside>
    <!--sidebar end-->
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-4 col-xs-4">

                </div>
                <div id="display" name="display" class="col-md-5 col-xs-5" style="margin-top: 25px;">
                    <?php
                    if (isset($_POST['edit_website'])) {
                        $old_url = $_POST['old_added_url'];
                        $url = $_POST['added_url'];
                        $tag = $_POST['added_url_tag'];
                        $query = "UPDATE Url_Status SET Url = '" . $url . "',Url_Tag = '" . $tag . "' WHERE Url = '" . $old_url . "';";
                        $result = mysqli_query($conn, $query);
                        if ($result) {
                            $msg = "Url Updated.";
                        } else {
                            $msg = "Url Not Updated.";
                        }
                        //error_log($query);
                    }
                    ?>
                    <strong style="color:#5CB85C;"><?php if (isset($msg)) {
                            echo $msg;
                        } ?></strong>

                </div>
                <div class="col-md-3 col-xs-3">
                    <button class="btn btn-theme04" style="float:right; display:inline-block; margin-top:20px;"
                            data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-plus"></i>
                        ADD NEW URL
                    </button>
                </div>
            </div>
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="content-panel table-responsive ds">
                        <h3 style="margin-top: -15px; margin-bottom: 10px;">My Websites</h3>
                        <section id="unseen" style="margin-left: 10px; margin-right: 10px;">

                            <?php
                            $query = "select tag,url,DATE_FORMAT(convert_tz(user_url.date,'" . getOffset('America/Detroit') . "','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as date from user_url where user='" . $_SESSION['email'] . "';";
                           // echo $query;
                            // echo "select tag,url,DATE_FORMAT(convert_tz(date,'-4:00','" . getOffset($_SESSION['timezone']) . "'),'%e %b %Y %h:%i %s %p') as date from user_url where user='" . $_SESSION['email'] . "'";
                            $result = mysqli_query($conn, $query);
                            $i = 1;
                            if (mysqli_num_rows($result) != 0) {
                                ?>
                                <table class="table table-bordered table-striped table-condensed" id="all_website">
                                    <thead>
                                    <tr>
                                        <!--                                        <th>S. No.</th>-->
                                        <th>Website Name</th>
                                        <th>URL</th>
                                        <th>Added On</th>
                                        <th>Details</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                        //     echo $data['tag'];
                                        echo "<tr><td><a target=\"_blank\" href=\"" . $data['url'] . "\">" . $data['tag'] . "</a></td><td><a target=\"_blank\" href=\"" . $data['url'] . "\">" . $data['url'] . "</a></td><td>" . $data['date'] . "</td><td  id=\"" . $i . "\" ><a class=\"btn btn-success\" href=\"websitedetails.php?url=" . $data['url'] . "\" target=\"_blank\" style=\"color:white;\"><i class=\"fa fa-info\"></i> Details</a></td><td><button id=\"" . $data['url'] . "\" class=\"btn btn-danger\" onclick=\"delete_url(" . $i . ",this.id)\"><i class=\"fa fa-trash-o\"></i> Delete</button></td></tr>";
                                        $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <?php
                            } else {
                                echo "No Website Found.Please Add First.";
                            }
                            ?>


                        </section>
                    </div>
                    <!-- /content-panel -->
                </div>
                <!-- /col-lg-4 -->
            </div>
            <!-- /row -->


        </section>
        <! --/wrapper -->
    </section>
    <!-- /MAIN CONTENT -->

    <!--main content end-->
    <!--footer start-->
    <?php
    include 'footer.php';
    ?>
    <!--footer end-->
</section>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/common-scripts.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script
</body>
</html>
