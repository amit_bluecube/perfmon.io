<footer class="site-footer">
    <div class="text-center">
        Copyright &copy; 2015 <a href="#">Web Awake</a>
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>