<?php
include 'connection.php';
include 'email.php';
include 'function.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Awake | Forgot Password</title>
    <link rel="stylesheet" href="assets/css/style_login.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
</head>
<body id="body_identity" class="align">
<div class="col-sm-offset-4"></div>
<div class="col-xs-8 col-sm-4">


        <?php
        if (isset($_POST['submit'])) {
            if (isset($_POST['email'])) {
                $email = $_POST['email'];
                $query = "select name from user_details where email='" . $email . "'";
                $result = mysqli_query($conn, $query);
                if (mysqli_num_rows($result)) {
                    while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                        $name = $data['name'];
                        $token = rand(100000, 999999);
                        $query2 = "delete from password_change_requests where email='" . $email . "'";
                        $result2 = mysqli_query($conn, $query2);
                        if ($result2) {
                            $query1 = "insert into password_change_requests (email,token) values('" . $email . "','" . $token . "');";
                            $result1 = mysqli_query($conn, $query1);
                            if ($result1) {
                                $url = "http://www.perfmon.io/recoverpassword.php?id=" . password_hash($token, PASSWORD_BCRYPT, ['cost' => 15]) . "&uid=" . $email;
                                $mail = sendPasswordChangeLink("noreply@perfmon.io", "BlueCube Network", $email, $name, $url);
                                if ($mail) {?>
                                    <h4 align="center" style="color:#4DBF4D;"><?php echo "A link to reset password has been sent to your email. Please check your email."; ?></h4>
                                    <!--echo "A link to reset password has been sent to your email. Please check your email.";-->
                               <?php }
                            } else {
                                //inserting data into password_change_requests failed
                                echo "Error";
                            }
                        } else {
                            //unable to delete old password change requests.
                            echo "Error";
                        }
                    }
                } else {
                    //email not found
                    echo "Email Not Found In Our Database.";
                }
            }
        }else{
            ?>
    <h2>Forgot your password?</h2>

    <p>Not to worry. Just enter your email address below and we'll send you an instruction email for recovery.</p>

    <form action="resetpassword.php" method="post">
            <div class="form-group">
                <input name="email" id="email" type="email" required="required" placeholder="Email" class="form-control">
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-sm-offset-7">
                    <button class="btn btn-theme03" type="submit" name="submit" id="submit"
                            style="padding: 12px  24px; font-weight: bold;">Reset Password
                    </button>
                </div>
            </div>

            <?php
        }

        ?>
    </form>
</div>

</body>
</html>