<?php
session_start();
include 'connection.php';
include 'email.php';
include 'function.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: signin.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'signin.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'signin.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);

}
if (isset($_GET['url'])) {
    $url = $_GET['url'];
} else {

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="">
    <title>Website</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/css/Notify.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
    <link href="assets/css/table-responsive.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">


    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="highstock/highstock.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#down_history').DataTable();
        });

        $(function () {
            var processed_json = new Array();
            $.getJSON('s_w_data.json', function (data) {
                // Populate series
//alert("helo");
                for (i = 0; i < data.length; i++) {
                    var temp_year = parseInt(data[i].day.split("/")[0]);
                    var temp_month = parseInt(data[i].day.split("/")[1]);
                    var temp_day = parseInt(data[i].day.split("/")[2]);
//                    console.log(temp_year);
//                    console.log(temp_month);
//                    console.log(temp_day);
//                    console.log(Date.UTC(temp_year, temp_month, temp_day));
//                    console.log(data[i].per);
                    processed_json.push([Date.UTC(temp_year, temp_month, temp_day), data[i].per]);
                }
//                console.log(data.length);
                // draw chart
                $('#single_spline').highcharts('StockChart', {
                    chart: {
                        type: "areaspline"
                    },
                    title: {
                        text: "WWW.XYZ.COM"
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        dateTimeLabelFormats: {
                            day: '%e of %b %Y'
                        },
                        valueSuffix: ' % UP'
                    },
                    rangeSelector: {
                        selected: 1,
                        allButtonsEnabled: true,
                        buttons: [{
                            type: 'month',
                            count: 1,
                            text: '1m'
                        }, {
                            type: 'month',
                            count: 3,
                            text: '3m'
                        }, {
                            type: 'month',
                            count: 6,
                            text: '6m'
                        }, {
                            type: 'ytd',
                            text: 'YTD'
                        }, {
                            type: 'year',
                            count: 1,
                            text: '1y'
                        }, {
                            type: 'all',
                            text: 'All'
                        }]
                    },
                    scrollbar: {
                        barBackgroundColor: '#FF865C',
                        barBorderRadius: 7,
                        barBorderWidth: 0,
                        buttonBackgroundColor: 'gray',
                        buttonBorderWidth: 0,
                        buttonBorderRadius: 7,
                        trackBackgroundColor: 'none',
                        trackBorderWidth: 1,
                        trackBorderRadius: 8,
                        trackBorderColor: '#CCC',
                    },
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: {
                            day: '%e  %b'
                        },
                        title: {
                            text: "Date"
                        },

                    },
                    scroolbar: {
                        enabled: true,
//                        minWidth:8*60*60*1000*3000
                    },
                    yAxis: {

                        minPadding: 0,
                        maxPadding: 0,
                        min: 0,
                        max: 100,
                        title: {
                            text: "UpTime Percentage"
                        },
//                        showLastLabel: false,
                        tickInterval: 10,
//

                        labels: {
                            formatter: function () {
                                return this.value + ' %';
                            },
                            align: 'left'
                        },
//                        minRange: 1

                    },
                    series: [{
                        name: 'www.xyz.com',
                        data: processed_json,
                        color: '#68DFF0'
                    }]
                });
            });
        });
        $(function () {
            //alert("Hello");
            $('#single_pie').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: '<?php echo $url;?> Up-Down Till Now'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                <?php
                 $down=getDownPercentage($url, $conn, $_SESSION['email']);
                 $up=100-$down;
                   if($down>100){
                    $down=100;
                    $up=0;
                 }
                 if($up>100){
                 $up=100;
                 $down=0;
                 }
                 ?>
                series: [{
                    name: "<?php echo $url;?>",
                    colorByPoint: true,
                    data: [{
                        name: "Up",
                        y:<?php echo $up;?>,
                        color: '#46BFBD',
                    }, {
                        name: "Down",
                        y: <?php echo $down ;?>,
                        color: '#F14955',
                        sliced: true,
                        selected: true
                    }]
                }]
            });
        });
        $(function () {
            $('#single_bar').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Weekly Report for www.xyz.com'
                },
                subtitle: {
                    text: 'Data are approximated and rounded.'
                },
                xAxis: {
                    categories: [
                        'Sunday',
                        'Monday',
                        'Tuesday',
                        'Wednesday',
                        'Thrusday',
                        'Friday',
                        'Saturday'
                    ],
                    crosshair: true
                },
                yAxis: {
                    minPadding: 0,
                    maxPadding: 0,
                    tickInterval: 6,
                    min: 0,
                    max: 24,
                    title: {
                        text: 'Up/Down Time (In Hour)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Hour</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Up',
                    color: '#46BFBD',
                    data: [20, 13, 24, 23, 23.5, 12, 18]

                }, {
                    name: 'Down',
                    color: '#F14955',
                    data: [4, 11, 0, 1, .5, 12, 6]

                }]
            });
        });
    </script>
</head>
<body>
<section id="container">
    <?php
    include 'header.php';
    include 'sidebar.php';
    include 'Add_Url.php';

    ?>

    <section id="main-content">
        <section class="wrapper site-min-height">
            <!-- <h3><i class="fa fa-angle-right"></i> <?php /*echo $url; */ ?></h3>-->

            <div id="chartjs" class="tab-pane">
                <div class="row mt">
                    <div class="col-md-12">
                        <div class="content-panel table-responsive ds">
                            <h3 style="margin-top: -15px; margin-bottom: 10px; font-weight:800;"> Down History
                                of <?php echo $url; ?></h3>
                            <section id="unseen">


                                <?php
                                $query = "SELECT down_history.start as start,down_history.url as url ,down_history.till as till,down_history.status as status,down_history.down_id as down_id,TIMESTAMPDIFF(MINUTE,down_history.start,down_history.till) as diff FROM down_history left JOIN user_url on down_history.start >= user_url.date where user_url.user = '" . $_SESSION['email'] . "' AND user_url.url =  '" . $url . "' and down_history.url='" . $url . "';";
                                //   $query = "select down_id,DATE_FORMAT(start,'%e %b %Y %h:%i %s %p') as start,DATE_FORMAT(till,'%e %b %Y %h:%i %s %p') as till,status,TIMESTAMPDIFF(MINUTE,start,till) as diff from down_history where url='" . $url . "'";
                                //   echo $query;
                                $result = mysqli_query($conn, $query);
                                if (mysqli_num_rows($result) != 0) {
                                    ?>
                                    <table class="table table-bordered table-striped table-condensed" id="down_history">
                                        <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>From</th>
                                            <th>Till</th>
                                            <th>Down Time Duration</th>
                                            <th>Reason</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 1;
                                        while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                            echo "<tr><td>" . $i . "</td><td>" . $data['start'] . "</td><td>" . $data['till'] . "</td><td>" . $data['diff'] . " Min</td><td>" . $data['status'] . "</td></tr>";
                                            $i++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    echo "No History Found.";
                                }
                                ?>

                            </section>
                        </div>
                    </div>

                </div>
                <div class="row mt">
                    <div class="col-lg-6">
                        <div class="content-panel ds">
                            <h3 style="margin-top: -15px; margin-bottom: 10px; background-color:#46BFBD;">Up Down
                                Record(%)</h3>

                            <div class="panel-body text-center" id="single_pie">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-panel ds">
                            <h3 style="margin-top: -15px;background-color:#fcb322;"> Latest Notifications</h3>

                            <div class="panel-body ">
                                <ul class="dashboard-list metro">
                                    <?php
                                    $query = "select text,DATE_FORMAT(time,'%e %b %Y %h:%i %s %p') as time from notification where user='" . $_SESSION['email'] . "' and text LIKE '%" . $url . "%' order by time desc LIMIT 5;";
                                    $result = mysqli_query($conn, $query);
                                    if (mysqli_num_rows($result) != 0) {
                                        ?>

                                        <?php
                                        while ($data = mysqli_fetch_array($result)) {
                                            echo "<li style=\"padding-bottom: 20px; padding-top: 20px;\">

    <i class=\"" . getClass($data['text']) . "\" style=\"float:left;\"></i>

    <a href=\"#\">" . $data['text'] . " on " . $data['time'] . ".</a >
</li > ";
                                        }

                                    } else {
                                        echo "No Notification Found";
                                    }
                                    ?>

                                    <li style="padding-bottom: 20px; padding-top: 20px;text-align: right;">
                                        <a href="shownotification.php">
                                            See more <i class="fa fa-arrow-right "></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </section>
    <! --/wrapper -->
</section>
<?php
include 'footer.php';
?>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="assets/js/common-scripts.js"></script>
<script>
    $(function () {
        $('select.styled').customSelect();
    });

</script>
</body>
</html>
