<?php
/**
 * Created by PhpStorm.
 * User: Rahul Jha
 * Date: 1/11/14
 * Time: 11:27 PM
 */
session_start();
session_unset();
session_destroy();

header("location:signin.php");
exit();
?>