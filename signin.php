<?php
session_start();
include 'connection.php';
include 'function.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Awake | Login</title>
    <link rel="stylesheet" href="assets/css/style_login.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <script src="sweet_alert/sweetalert.min.js"></script>
    <script>
        function loginfb() {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '949732471752416',
                cookie: true,
                xfbml: true,
                version: 'v2.2'
            });
          getLoginStatus();
        };

        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

            function getLoginStatus() {
                FB.getLoginStatus(function (status_response) {
                    if (status_response.status === 'connected') {
                        console.log("Facebook Connected.");
                        var uid = status_response.authResponse.userID;
                        // console.log("Current UserID= " + uid);
                        FB.api('/me', {fields: 'email,first_name,last_name'}, function (response) {
                            console.log(response);
                            // console.log(JSON.stringify(response));
                            var uid = response.id;
                            var first_name = response.first_name;
                            var last_name = response.last_name;
                            var name = first_name + " " + last_name;
                            var email = response.email;
                            if (uid != undefined && first_name != undefined && last_name != undefined && email != undefined) {
                                //login the user
                                swal("Logging You In Via Facebook, Please Wait..");
                                $.ajax({
                                    type: "POST",
                                    url: 'fb_handler.php',
                                    data: {"email": email, "name": name, "uid": uid},
                                    success: function (data) {
                                        console.log(data);
                                        //     console.log(data.action);
                                        var dataa = JSON.parse(data);
                                        console.log(dataa.name);
                                        console.log(dataa.email);
                                        console.log(dataa.action);
                                        if (dataa.action == "login" || dataa.action == "signup") {
                                            window.location.assign("dashboard.php");
                                        }
                                    },
                                    error: function (jqXHR, exception) {
                                        if (jqXHR.status === 0) {
                                            swal("Not connection", "Not connection ,Verify Network.", "error");
                                        } else if (jqXHR.status == 404) {
                                            swal("Not Found", "Requested page not found. [404]", "error");
                                        } else if (jqXHR.status == 500) {
                                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                                        } else if (exception === 'parsererror') {
                                            swal("parse error", "Requested JSON parse failed.", "error");
                                        } else if (exception === 'timeout') {
                                            swal("Request Timeout", "Time out error.", "error");
                                        } else if (exception === 'abort') {
                                            swal("Request Aborted", "Ajax request aborted.", "error");
                                        } else {
                                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                                        }
                                    }

                                });
                            }
                        });

                    } else if (status_response.status === 'not_authorized') {
                        console.log("the user is logged in to Facebook but has not authenticated my app");
                        FB_SIGNIN();
                    } else {
                        console.log("the user doesn't logged in to Facebook.");
                        FB_SIGNIN();
                    }
                });
            }
        }
        function FB_SIGNIN() {
            console.log('Welcome!  Fetching your information.... ');
            FB.login(function (login_response) {
                if (login_response.status === 'connected') {
                    // Logged into your app and Facebook.
                    console.log(login_response.authResponse.accessToken);
                    FB.api('/me', {fields: 'email,first_name,last_name'}, function (response) {
                        console.log(response);
                        var email = response.email;
                        var id = response.id;
                        var first_name = response.first_name;
                        var last_name = response.last_name;
                        var name = first_name + " " + last_name;
                        if (email != undefined && first_name != undefined && last_name != undefined && id != undefined) {
//login the user
                            swal("Logging You In Via Facebook, Please Wait..");
                            $.ajax({
                                type: "POST",
                                url: 'fb_handler.php',
                                data: {"email": email, "name": name, "uid": uid},
                                success: function (data) {
                                    console.log(data);
                                    // console.log(data.action);
                                    var dataa = JSON.parse(data);
                                    console.log(dataa.name);
                                    console.log(dataa.email);
                                    console.log(dataa.action);
                                    if (dataa.action == "login" || dataa.action == "signup") {
                                        window.location.assign("dashboard.php");
                                    }
                                },
                                error: function (jqXHR, exception) {
                                    if (jqXHR.status === 0) {
                                        swal("Not connection", "Not connection ,Verify Network.", "error");
                                    } else if (jqXHR.status == 404) {
                                        swal("Not Found", "Requested page not found. [404]", "error");
                                    } else if (jqXHR.status == 500) {
                                        swal("Internal Server Error", "Internal Server Error [500].", "error");
                                    } else if (exception === 'parsererror') {
                                        swal("parse error", "Requested JSON parse failed.", "error");
                                    } else if (exception === 'timeout') {
                                        swal("Request Timeout", "Time out error.", "error");
                                    } else if (exception === 'abort') {
                                        swal("Request Aborted", "Ajax request aborted.", "error");
                                    } else {
                                        swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                                    }
                                }

                            });
                        }
                    });
                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                }

            }, {scope: 'public_profile,email'});
        }
    </script>
    <style>
        #login {
            border: 1px solid #505050;
            padding-bottom: 20px;
        }

        #legend {
            display: block;
            font-size: 21px;
            line-height: inherit;
            margin-bottom: 20px;
            padding: 0;
            width: 24%;
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: -moz-use-text-color -moz-use-text-color #e5e5e5;
            border-image: none;
            border-style: none none solid;
            border-width: 0;
            color: #A9A9A9;
            margin-top: 15px;
            margin-left: 20px;
        }
    </style>
</head>
<body id="body_identity" class="align">
<div class="col-lg-8 col-lg-offset-2">
    <fieldset id="login">

        <legend id="legend">Sign in Your Account</legend>

        <div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1">
            <div class="sns-signin ">
                <a href="#" onclick="loginfb()" class="btn btn-primary" style="margin-top: 52%;">
                    <i class="fa fa-facebook"></i>Continue With Facebook</a>
            </div>

            <!--<div class="sns-signin ">
                <a href="#" class="btn btn-theme04">
                    <i class="fa fa-google-plus"></i>Sign in via Google Plus</a>
            </div>

            <div class="sns-signin ">
                <a href="#" class="btn btn-primary" style="background-color: #0072C6">
                    <i class="fa fa-windows"></i>Sign in via Outlook</a>
            </div>-->
        </div>
        <div class="col-xs-12 col-md-2 col-sm-1">
            <div class="horizontal-divider"></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="normal-signup">
                <form name="login_form" id="login_form" action="signin.php" method="post">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8" style="margin-top:90px;float:right;">
                                <span>Create an Account? <a href="signup.php"> Sign Up</a></span>
                            </div>
                        </div>
                        <input type="email" required="required" placeholder="Email" name="email" id="email"
                               class="form-control">
                        <input type="password" required placeholder="Password" id="password" name="password"
                               class="form-control">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="resetpassword.php" id="forgot_from_3">Forgot Password?</a>
                        </div>
                        <div class="col-md-5 col-md-offset-7">
                            <button class="btn btn-theme04" name="submit" id="submit" type="submit">Sign In</button>
                        </div>

                    </div>
                    <?php
                    if (isset($_POST['submit'])) {
                        if (isset($_POST['email']) && isset($_POST['password'])) {
                            $email = mysqli_real_escape_string($conn, trim($_POST['email']));
                            $password = mysqli_real_escape_string($conn, trim($_POST['password']));
                            $query = "select password from users where email='" . $email . "'";
                            //  echo $query . "<br>";
                            //echo $password . "<br>";
                            $result = mysqli_query($conn, $query);
                            $database_password = "";
                            if (mysqli_num_rows($result) != 0) {
                                while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                    $database_password = $data['password'];
                                    if (password_verify($password, $database_password)) {
                                        $query1 = "select name,time_zone,profile_pic from user_details where email='" . $email . "'";
                                        $result1 = mysqli_query($conn, $query1);
                                        if (mysqli_num_rows($result1) != 0) {
                                            while ($data1 = mysqli_fetch_array($result1, MYSQLI_BOTH)) {
                                                $_SESSION['name'] = $data1['name'];
                                                $_SESSION['timezone'] = $data1['time_zone'];
                                                $_SESSION['email'] = $_POST['email'];
                                                $_SESSION['profile_pic'] = $data1['profile_pic'];
                                            }


                                        } else {
                                            //failed to initialize your session
                                            echo "failed to initialize session";
                                            die();
                                        }
                                        if (!headers_sent()) {
                                            header('Location:dashboard.php');
                                            exit;
                                        } else {
                                            echo '<script type="text/javascript">';
                                            echo 'window.location.href="' . 'dashboard.php' . '";';
                                            echo '</script>';
                                            echo '<noscript>';
                                            echo '<meta http-equiv="refresh" content="0;url=' . 'dashboard.php' . '" />';
                                            echo '</noscript>';
                                            exit;
                                        }
                                    } else {
                                        //wrong password
                                        echo '<p style="color: #D43037; font-size:15px; margin-top: 5px;" align="center"><i class="fa fa-exclamation-triangle"></i>
                                    Authentication Failed</p>';
                                    }
                                }
                            } else {
                                //email doesn't exist.
                                echo '<p style="color: #D43037; font-size:15px; margin-top: 5px;" align="center"><i class="fa fa-exclamation-triangle"></i>
                                    Unknown User Credentials</p>';

                            }
                        } else {
                            echo '<p style="color: #D43037; font-size:15px; margin-top: 5px;" align="center"><i class="fa fa-exclamation-triangle"></i>
                                    FIll all the required Fields </p>';//fill all fields;

                        }
                    } else {
                    }
                    ?>
                </form>
            </div>
        </div>
    </fieldset>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</body>
</html>