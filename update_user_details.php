<?php
session_start();
include 'connection.php';
if (isset($_POST['new_name'])) {
    $name = $_POST['new_name'];
    $query = "UPDATE user_details SET user_details.name = '" . $name . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
    $result = mysqli_query($conn, $query);
    if ($result) {
        $_SESSION['name'] = $name;
        echo "Name updated successfully.";
    } else {
        echo "Error,Something is Wrong!";
    }
}
if (isset($_POST['old_password']) && isset($_POST['new_password']) && isset($_POST['re_new_password'])) {
    $old_password = $_POST['old_password'];
    $new_password = $_POST['new_password'];
    $re_new_password = $_POST['re_new_password'];
    if ($new_password != $re_new_password) {
        echo "Password Mismatch";
    } else {
        $query = "select password from users where email='" . $_SESSION['email'] . "';";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) == 0) {
            echo "Wrong Password";
        } else {
            while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                if (password_verify($old_password, $data['password'])) {
                    $query = "UPDATE users SET password = '" . password_hash($new_password, PASSWORD_BCRYPT, ['cost' => 15]) . "' WHERE users.email = '" . $_SESSION['email'] . "';";
                    $result = mysqli_query($conn, $query);
                    if ($result) {
                        echo "Password Changed Successfully";
                    } else {
                        echo "Password Changing Failed";
                    }
                } else {
                    echo "Wrong Password";
                }
            }
        }
    }
}
/*if (isset($_POST['email'])) {
    $email = $_POST['email'];
    error_log(filter_var($email, FILTER_VALIDATE_EMAIL));
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $query = "UPDATE users SET email = '" . $email . "' WHERE users.username = '" . $_SESSION['username'] . "';";
        $result = mysqli_query($conn, $query);
        if ($result) {
            $_SESSION['email'] = $email;
            echo "Email updated successfully.";
        } else {
            echo "Error,Something is Wrong!";
        }
    } else {
        echo "OOPS! We Accepts Valid Email Only.";
    }
}*/
if (isset($_POST['alt_email'])) {
    $alt_email = $_POST['alt_email'];
    if (filter_var($alt_email, FILTER_VALIDATE_EMAIL)) {
        $query = "UPDATE user_details SET secondary_email = '" . $alt_email . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
//error_log($query);
        $result = mysqli_query($conn, $query);
        if ($result) {
            echo "Alternate Email updated successfully.";
        } else {
            echo "Error,Something is Wrong!";
        }
    } else {
        echo "Incorrect Email.!";
    }
}
if (isset($_POST['contact_no'])) {
    $contact_no = $_POST['contact_no'];
    if (filter_var($contact_no, FILTER_VALIDATE_INT) && strlen($contact_no) == 10 && $contact_no > 0) {
        $query = "UPDATE user_details SET phone = '" . $contact_no . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
//error_log($query);
        $result = mysqli_query($conn, $query);
        if ($result) {
            $_SESSION['phone'] = $contact_no;
            echo "Contact Number updated successfully.";
        } else {
            echo "Error,Something is Wrong!";
        }
    } else {
        echo "Incorrect Phone Number!";
    }

}
if (isset($_POST['alt_contact_no'])) {
    $alt_contact_no = $_POST['alt_contact_no'];
    if (filter_var($alt_contact_no, FILTER_VALIDATE_INT) && strlen($alt_contact_no) == 10 && $alt_contact_no > 0) {
        $query = "UPDATE user_details SET alternate_phone = '" . $alt_contact_no . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
        $result = mysqli_query($conn, $query);
        if ($result) {
            echo "Secondary Contact Number updated successfully.";
        } else {
            echo "Error,Something is Wrong!";
        }
    } else {
        echo "Incorrect Phone Number!";
    }
}
if (isset($_POST['company_name'])) {
    $company_name = $_POST['company_name'];
    $query = "UPDATE user_details SET company_name = '" . $company_name . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
    $result = mysqli_query($conn, $query);
    if ($result) {
        echo "Company Name updated successfully.";
    } else {
        echo "Error,Something is Wrong!";
    }
}
if (isset($_POST['profile_pic'])) {

}
if (isset($_POST['job_profile'])) {
    $job_profile = $_POST['job_profile'];
    $query = "UPDATE user_details SET job_profile = '" . $job_profile . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
    $result = mysqli_query($conn, $query);
    if ($result) {
        echo "Job Profile updated successfully.";
    } else {
        echo "Error,Something is Wrong!";
    }
}
if (isset($_POST['timezone'])) {
    $timezone = $_POST['timezone'];
   // echo $timezone;
    $query = "UPDATE user_details SET time_zone = '" . $timezone . "' WHERE user_details.email = '" . $_SESSION['email'] . "';";
   // echo $query;
    $result = mysqli_query($conn, $query);
    if ($result) {
        $_SESSION['timezone']=$timezone;
        echo "TimeZone updated successfully.";

    } else {
        echo "Error,Something is Wrong!";
    }
}
?>
