<?php
include 'connection.php';

?>
<!--edit website modal starts here--->
<div class="modal fade" id="web_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#31C0D5">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-pencil-square-o"></i>
Edit Your Website</h4>
            </div>
            <form id="Web_edit_form" action="table.php" method="post" class="form form--login">
                <div class="modal-body">
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Edit Url</label>
                        <input id="old_added_url" type="hidden" name="old_added_url">
                        <input type="url" value="<?php echo $_SESSION[edit_url]; ?>" class="form-control" id="added_url" name="added_url" placeholder="Added URL" required="required">
                        e.g. https://www.example.xyz
                    </div>
                    <div class="form-group has-success">
                        <label class="control-label" for="inputSuccess">Edit Website Name</label>
                        <input type="text" value="<?php echo $_SESSION[$url_name];  ?>" class="form-control" id="added_tag" name="added_url_tag"  required="required" placeholder="Your Website name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="edit_website" name="edit_website" class="btn btn-info">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--edit website modal ends here-->