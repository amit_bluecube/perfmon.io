<?php
include 'connection.php';
include 'function.php';
include 'email.php';
//error_reporting(E_ALL);
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['re_password']) && isset($_POST['timezone'])) {
    /*if(!isEmail($_POST['email'])){
        echo "Invalid Email!";
        die();
    }*/
    $name = mysqli_escape_string($conn, trim($_POST['name']));
    $password = mysqli_real_escape_string($conn, trim($_POST['password']));
    $email = mysqli_real_escape_string($conn, trim($_POST['email']));
//    echo $_POST['email'] . "<br>";
//    echo $email . "<br>";
    $re_password = mysqli_real_escape_string($conn, trim($_POST['re_password']));
//    echo $password . "<br>";
    $timezone = mysqli_real_escape_string($conn, trim($_POST['timezone']));
    if ($password == $re_password) {
        mysqli_autocommit($conn, false);
        $query1 = "insert into users (email,password) VALUES ('" . $email . "','" . password_hash($password, PASSWORD_BCRYPT, ['cost' => 15]) . "');";
//        echo($query1);
        $query2 = "insert into signup_type (email,type) VALUES ('" . $email . "','manual');";
//        echo($query2);
        $query3 = "insert into user_details(name,email,time_zone,account_created_on) values ('" . $name . "','" . $email . "','" . $timezone . "',now());";
        // echo($query3);
        $result1 = mysqli_query($conn, $query1);
        $result2 = mysqli_query($conn, $query2);
        $result3 = mysqli_query($conn, $query3);
        if ($result1 && $result2 && $result3) {
            mysqli_commit($conn);
            echo "Success! Account Created. You Can Login Now.";
            $notification = "Welcome " . $name . ", Good To See You Here.";
            $notified = notify($notification, $email, $conn);
            if ($notified) {
                //good
            } else {
                error_log($notified);
            }
            $email_sent = email_account_created('noreply@perfmon.io', 'perfmon.io', $_POST['email'], $_POST['name']);
            if ($email_sent) {
                //echo("Account of ".$_POST['name']." Created.");
            } else {
                // echo($email_sent);
            }
        } else {
//            echo(mysqli_error($conn));
            mysqli_rollback($conn);
            echo "Error! Something Went Wrong.";
        }
    }
} else {
    echo "Check That If All Field Are Filled Properly.";
}
mysqli_autocommit($conn, true);
?>