<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">

            <p class="centered"><a href="profile.php"><img src="<?php echo $_SESSION['profile_pic']; ?>" class="img-circle" width="60"></a></p>
            <h5 class="centered"><?php echo $_SESSION['name']?></h5>

            <li class="mt">
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-th"></i>
                    <span>Show all website</span>
                </a>
                <ul class="sub">
                    <li><a  href="websites.php"><i class="fa fa-table"></i> My Websites</a></li>
                    <li><a  href="downhistory.php"><i class="fa fa-bar-chart"></i> Show Log</a></li>

                </ul>
            </li>
            <li class="sub-menu" data-toggle="modal" data-target="#myModal">
                <a href="javascript:;" >
                    <i class="fa fa-plus-circle"></i>
                    <span>Add new URL</span>
                </a>
            </li>
            <li id="report-bug">
                <a href="report-bug.php">
                    <i class="fa fa-bug"></i>
                    <span>Report Bug</span>
                </a>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->