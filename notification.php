<?php
session_start();
include 'connection.php';
include 'email.php';
include 'function.php';
if (!isset($_SESSION['email'])) {
    if (!headers_sent()) {
        header("location: sign_in.php");
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . 'login.php' . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . 'login.php' . '" />';
        echo '</noscript>';
        exit;
    }
} else {
//    error_log($_SESSION['name']);

}
if (isset($_GET['url'])) {
    $url = $_GET['url'];
} else {

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Notifications</title>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <style>
        .mark_icon {
            /*  border: 1px solid transparent;
              border-radius: 10px;*/
            color: #999;
            line-height: 17px;
            padding: 0 3px;
            float: right;
            display: inline-block;
        }

        .mark_icon:hover {
            border-color: #2b6dad;
            color: #2b6dad;
            text-decoration: none;
        }


    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="sweet_alert/sweetalert.min.js"></script>
</head>
<body style="background-color: white;">
<div class="col-lg-12 col-xs-12">
    <ul>
        <?php
        $query = "SELECT id,text, time FROM notification WHERE user = '" . $_SESSION['email'] . "' and status='NR' ORDER BY time DESC LIMIT 0 , 14";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) != 0) {
            while ($data = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                ?>
                <li style="padding-bottom: 20px; padding-top: 20px; margin-left: -15%; " class="notification_list"
                    id="list_<?php echo $data['id']; ?>">

                    <a href="#"><?php echo $data['text'] . " at " . $data['time'] . " ."; ?>
                        <div class="mark_icon">
                            <button class="btn btn-danger btn-xs"  onclick="remove_notification(this.id)"
                                    id="<?php echo $data['id']; ?>">&times;</button>
                        </div>
                    </a>
                </li>

                <?php
            }
        } else {
            echo "No New Notification";
        }
        ?>

<!--        <li style="padding-bottom: 20px; padding-top: 20px; margin-left: -15%; " class="notification_list">-->
<!--            <a href="http://perfmon.io/show-notifications.php"> Show all Notifications </a>-->
<!--        </li>-->

    </ul>
</div>

<script src="assets/js/bootstrap.min.js"></script>
<script>
    function remove_notification(id) {
//        alert(id);
        $.ajax({
            type: "POST",
            url: 'mark_notification_as_read.php',
            data: "id=" + id,
            success: function (data) {
                //   swal(data);// alert the data from the server
                var list_id = "list_" + id;
                $("#list_" + id).hide();
                $("#list_" + id).attr("display", "none");
                document.getElementById("list_" + id).style.display = "none"
                if (data == "Done") {
                    //swal(data);// alert the data from the server
                } else {
                    // swal(data);// alert the data from the server
                }
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    swal("Not connection", "Not connection ,Verify Network.", "error");
                } else if (jqXHR.status == 404) {
                    swal("Not Found", "Requested page not found. [404]", "error");
                } else if (jqXHR.status == 500) {
                    swal("Internal Server Error", "Internal Server Error [500].", "error");
                } else if (exception === 'parsererror') {
                    swal("parse error", "Requested JSON parse failed.", "error");
                } else if (exception === 'timeout') {
                    swal("Request Timeout", "Time out error.", "error");
                } else if (exception === 'abort') {
                    swal("Request Aborted", "Ajax request aborted.", "error");
                } else {
                    swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                }
            }

        });
    }
</script>
</body>
</html>
