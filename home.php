<!DOCTYPE html>
<html>
<head>
    <title>Web Awake</title>
    <!-- Style Sheet -->
    <link href="assets/css/home-style.css" rel="stylesheet" type="text/css">
    <!-- Style Sheet -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!----bootstrap css--->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    
    <!----bootstrap css--->

    <!----bootstrap js--->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!----bootstrap js--->

    <!----font awesome--->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!----font awesome--->

    <!--Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>
    <!--Google Fonts-->

</head>
<body style="background-color: #2c3338;">
<nav class="navbar navbar-default ">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="color: white">Web Awake</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Sign Up</a></li>
                <li><a href="#"> Sign In</a></li>
            </ul>
        </div>
    </div>
</nav>
<!--search start here-->
<div class="search">

    <i> </i>

    <div class="s-bar">
        <h3>Test the Site you think is DOWN!!</h3>

        <form action="#" method="post">
            <input type="url" placeholder="Enter your URL">
            <button type="button" data-toggle="modal" data-target="#result">Test Now</button>
        </form>

    </div>
</div>

<!-- Modal -->
<div id="result" class="modal fade" role="dialog">
    <div class="modal-dialog" style="color: black">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style=" background-color: #68DFF0; color: white">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" align="center" >Here is Your Result</h4>
            </div>
            <div class="modal-body" align="center">
                <h3>Your Site is Up <i class="fa fa-arrow-up"></i> / Down <i class="fa fa-arrow-down"></i>
                </h3>
            </div>
            <div class="modal-footer">
                <h5 style="float: left">To Regularly Monitor Your site Sign Up here. </h5> <button style="float: right; margin-top: 5px;" type="button" class="btn btn-info btn-sm">Sign Up</button>
            </div>
        </div>

    </div>
</div>
<!-----modal ends---->

<!--search end here-->
<div class="copyright">
    <p>Copyright &copy; 2015 <a href="#">Web Awake</a></p>
</div>
</body>
</html>