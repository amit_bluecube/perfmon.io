<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Awake | Create Account</title>
    <link rel="stylesheet" href="assets/css/style_login.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="sweet_alert/sweetalert.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="sweet_alert/sweetalert.min.js"></script>
    <script>
        function loginfb() {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '949732471752416',
                cookie: true,
                xfbml: true,
                version: 'v2.2'
            });
            getLoginStatus();
        };

        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

            function getLoginStatus() {
                FB.getLoginStatus(function (status_response) {
                    if (status_response.status === 'connected') {
                        console.log("Facebook Connected.");
                        var uid = status_response.authResponse.userID;
                        console.log("Current UserID= " + uid);
                        var accessToken = status_response.authResponse.accessToken;
                        console.log("Access Token= " + accessToken);
                        FB.api('/me', {fields: 'email,first_name,last_name'}, function (response) {
                            console.log(response);
                            var uid = response.id;
                            var first_name = response.first_name;
                            var last_name = response.last_name;
                            var name = first_name + " " + last_name;
                            var email = response.email;
                            if (uid != undefined && first_name != undefined && last_name != undefined && email != undefined) {
                                //login the user
                                swal("Logging You In Via Facebook, Please Wait..");
                                $.ajax({
                                    type: "POST",
                                    url: 'fb_handler.php',
                                    data: {"email": email, "name": name, "uid": uid},
                                    success: function (data) {
                                        console.log(data);
                                        //     console.log(data.action);
                                        var dataa = JSON.parse(data);
                                        console.log(dataa.name);
                                        console.log(dataa.email);
                                        console.log(dataa.action);
                                        if (dataa.action == "login" || dataa.action == "signup") {
                                            window.location.assign("dashboard.php");
                                        }
                                    },
                                    error: function (jqXHR, exception) {
                                        if (jqXHR.status === 0) {
                                            swal("Not connection", "Not connection ,Verify Network.", "error");
                                        } else if (jqXHR.status == 404) {
                                            swal("Not Found", "Requested page not found. [404]", "error");
                                        } else if (jqXHR.status == 500) {
                                            swal("Internal Server Error", "Internal Server Error [500].", "error");
                                        } else if (exception === 'parsererror') {
                                            swal("parse error", "Requested JSON parse failed.", "error");
                                        } else if (exception === 'timeout') {
                                            swal("Request Timeout", "Time out error.", "error");
                                        } else if (exception === 'abort') {
                                            swal("Request Aborted", "Ajax request aborted.", "error");
                                        } else {
                                            swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                                        }
                                    }

                                });
                            }
                            //   console.log(JSON.stringify(response));
                        });

                    } else if (status_response.status === 'not_authorized') {
                        console.log("the user is logged in to Facebook but has not authenticated your app");
                        FB_SIGNUP();
                    } else {
                        console.log("the user doesn't logged in to Facebook.");
                        FB_SIGNUP();
                    }
                });
            }
        }

        function FB_SIGNUP() {
         console.log('Welcome!  Fetching your information.... ');
         FB.login(function (login_response) {
         if (login_response.status === 'connected') {
         // Logged into your app and Facebook.
         //  console.log(login_response.authResponse.accessToken);
         FB.api('/me', {fields: 'email,first_name,last_name'}, function (response) {
         console.log(response);
         var uid = response.id;
         var first_name = response.first_name;
         var last_name = response.last_name;
         var name = first_name + " " + last_name;
         var email = response.email;
         if (uid != undefined && first_name != undefined && last_name != undefined && email != undefined) {
         //login the user
         swal("Logging You In Via Facebook, Please Wait..");
         $.ajax({
         type: "POST",
         url: 'fb_handler.php',
         data: {"email": email, "name": name, "uid": uid},
         success: function (data) {
         console.log(data);
         // console.log(data.action);
         var dataa = JSON.parse(data);
         console.log(dataa.name);
         console.log(dataa.email);
         console.log(dataa.action);
         if (dataa.action == "login" || dataa.action == "signup") {
         window.location.assign("dashboard.php");
         }
         },
         error: function (jqXHR, exception) {
         if (jqXHR.status === 0) {
         swal("Not connection", "Not connection ,Verify Network.", "error");
         } else if (jqXHR.status == 404) {
         swal("Not Found", "Requested page not found. [404]", "error");
         } else if (jqXHR.status == 500) {
         swal("Internal Server Error", "Internal Server Error [500].", "error");
         } else if (exception === 'parsererror') {
         swal("parse error", "Requested JSON parse failed.", "error");
         } else if (exception === 'timeout') {
         swal("Request Timeout", "Time out error.", "error");
         } else if (exception === 'abort') {
         swal("Request Aborted", "Ajax request aborted.", "error");
         } else {
         swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
         }
         }

         });
         }
         //   console.log(JSON.stringify(response));
         });
         } else if (response.status === 'not_authorized') {
         // The person is logged into Facebook, but not your app.
         } else {
         // The person is not logged into Facebook, so we're not sure if
         // they are logged into this app or not.
         }

         }, {scope: 'public_profile,email'});
         }
        function check_email_availablity() {
            var email = document.getElementById("email").value;
            $.ajax({
                type: "POST",
                url: 'check_email_availability.php',
                data: "email=" + email,
                success: function (data) {
                    if (data == "not_available") {
                        swal("Email Already Registered", "Email is Already Used In Another Account.", "error");
                    }
                },
                error: function (jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        swal("Not connection", "Not connection ,Verify Network.", "error");
                    } else if (jqXHR.status == 404) {
                        swal("Not Found", "Requested page not found. [404]", "error");
                    } else if (jqXHR.status == 500) {
                        swal("Internal Server Error", "Internal Server Error [500].", "error");
                    } else if (exception === 'parsererror') {
                        swal("parse error", "Requested JSON parse failed.", "error");
                    } else if (exception === 'timeout') {
                        swal("Request Timeout", "Time out error.", "error");
                    } else if (exception === 'abort') {
                        swal("Request Aborted", "Ajax request aborted.", "error");
                    } else {
                        swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");
                    }
                }

            });
        }
        function validate_form() {
            console.log("Validate form calling....");
            var name = document.getElementById("name").value;
            console.log(name);
            var email = document.getElementById("email").value;
            console.log(email);
            var password = document.getElementById("password").value;
            console.log(password);
            var re_password = document.getElementById("re_password").value;
            console.log(re_password);
            var time_zone = document.getElementById("timezone").value;
            console.log(time_zone);
            var re_captcha = grecaptcha.getResponse();
            console.log(re_captcha);
            var verified;
            /*   $.ajax({
             type: "POST",
             url: ' https://www.google.com/recaptcha/api/siteverify',
             data: {"secret": '6LfH5gsTAAAAAAvBIotiZPd7wNNjxOJ9rG_5ucrm', "response": 're_captcha'},
             success: function (isverified) {
             verified = isverified;
             console.log(verified);
             },
             error: function (jqXHR, exception) {
             if (jqXHR.status === 0) {
             swal("Not connection", "Not connection ,Verify Network.", "error");
             } else if (jqXHR.status == 404) {
             swal("Not Found", "Requested page not found. [404]", "error");
             } else if (jqXHR.status == 500) {
             swal("Internal Server Error", "Internal Server Error [500].", "error");
             } else if (exception === 'parsererror') {
             swal("parse error", "Requested JSON parse failed.", "error");
             } else if (exception === 'timeout') {
             swal("Request Timeout", "Time out error.", "error");
             } else if (exception === 'abort') {
             swal("Request Aborted", "Ajax request aborted.", "error");
             } else {
             swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");

             }
             }

             });*/
            if (re_captcha.length == 0) {
                swal("Are You Robot?", "Please Check Re-Captcha.", "error");
                return false;
            } else {
                grecaptcha.reset();
            }
            if (name.trim().lenght != 0 && email.trim().length != 0 && password.trim().length != 0 && re_password.trim().length != 0 && time_zone.trim().length != 0) {
                if (password == re_password) {
                    swal({
                            title: "Are You Sure?",
                            text: "Click OK to Proceed.",
                            type: "info",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        },
                        function () {
                            /*setTimeout(function(){
                             swal("Ajax request finished!");
                             }, 2000);*/
                            $.ajax({
                                type: "POST",
                                url: 'signup_handler.php',
                                data: {
                                    "email": email,
                                    "name": name,
                                    "password": password,
                                    "re_password": re_password,
                                    "timezone": time_zone
                                },
                                success: function (data) {
                                    swal(data);
                                    document.getElementById("signup_form").reset();
                                },
                                error: function (jqXHR, exception) {
                                    if (jqXHR.status === 0) {
                                        swal("Not connection", "Not connection ,Verify Network.", "error");
                                    } else if (jqXHR.status == 404) {
                                        swal("Not Found", "Requested page not found. [404]", "error");
                                    } else if (jqXHR.status == 500) {
                                        swal("Internal Server Error", "Internal Server Error [500].", "error");
                                    } else if (exception === 'parsererror') {
                                        swal("parse error", "Requested JSON parse failed.", "error");
                                    } else if (exception === 'timeout') {
                                        swal("Request Timeout", "Time out error.", "error");
                                    } else if (exception === 'abort') {
                                        swal("Request Aborted", "Ajax request aborted.", "error");
                                    } else {
                                        swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");

                                    }
                                }

                            });
                        });
                    /* $.ajax({
                     type: "POST",
                     url: 'signup_handler.php',
                     data: {"email": email, "name": name, "password": password,"re_password": re_password, "timezone": time_zone},
                     success: function (data) {
                     swal(data);
                     },
                     error: function (jqXHR, exception) {
                     if (jqXHR.status === 0) {
                     swal("Not connection", "Not connection ,Verify Network.", "error");
                     } else if (jqXHR.status == 404) {
                     swal("Not Found", "Requested page not found. [404]", "error");
                     } else if (jqXHR.status == 500) {
                     swal("Internal Server Error", "Internal Server Error [500].", "error");
                     } else if (exception === 'parsererror') {
                     swal("parse error", "Requested JSON parse failed.", "error");
                     } else if (exception === 'timeout') {
                     swal("Request Timeout", "Time out error.", "error");
                     } else if (exception === 'abort') {
                     swal("Request Aborted", "Ajax request aborted.", "error");
                     } else {
                     swal("Error!", "Uncaught Error.n" + jqXHR.responseText, "error");

                     }
                     }

                     });*/
                    return false;
                } else {
                    swal("Different Passwords?", "Please Make Sure That Both Password Fields Have Same Password.", "error");
                    return false;
                }
            } else {
                swal("Empty Fields?", "Please Fill All Fields..", "error");
                return false;
            }
        }
    </script>
    <style>
        #login {
            border: 1px solid #505050;
            padding-bottom: 20px;
        }

        #legend {
            display: block;
            font-size: 21px;
            line-height: inherit;
            margin-bottom: 20px;
            padding: 0;
            width:21%;
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: -moz-use-text-color -moz-use-text-color #e5e5e5;
            border-image: none;
            border-style: none none solid;
            border-width: 0;
            color: #A9A9A9;
            margin-top: 15px;
            margin-left: 20px;
        }
    </style>
</head>
<body id="body_identity" class="align">
<div class="col-lg-8 col-lg-offset-2">
    <fieldset id="login">
        <legend id="legend"> Create an Account</legend>
        <span>
            If you already have an Account
            <a href="signin.php">Sign In </a>here.
            </span>

        <div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1">
            <div class="sns-signin " style="margin-top: 52%;">
                <a href="#" onclick="loginfb()" class="btn btn-primary" style="margin-top: 100px;">
                    <i class="fa fa-facebook"></i>Continue With Facebook</a>
            </div>
            <!---Version 3 features not for cureent version-->
            <!--<div class="sns-signin ">

                <a href="#" class="btn btn-theme04">
                    <i class="fa fa-google-plus"></i>Sign Up via Google Plus</a>
            </div>

            <div class="sns-signin ">
                <a href="#" class="btn btn-primary" style="background-color: #0072C6">
                    <i class="fa fa-windows"></i>Sign Up via Outlook</a>
            </div>-->
        </div>
        <div class="col-xs-12 col-md-2 col-sm-1">
            <div class="horizontal-divider"></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="normal-signup">
                <form method="post" name="signup_form" id="signup_form" action="signin.php"
                      onsubmit="return validate_form()">
                    <div class="form-group">
                        <input type="text" required="required" placeholder="Name" id="name" class="form-control">
                        <input type="email" onblur="check_email_availablity()" id="email" name="email"
                               required="required"
                               placeholder="Email"
                               class="form-control">
                        <input type="password" required placeholder="Password" class="form-control" name="password"
                               id="password">
                        <input type="password" required placeholder="Confirm Password" class="form-control"
                               name="re_password"
                               id="re_password">
                        <select class="form-control" style="margin-bottom:20px;" id="timezone" name="timezone"></select>

                        <div class="g-recaptcha" data-sitekey="6Ldelw8TAAAAAHOkVw5WM7OHAlXc-jd2Rqxv7Xnv" id="re_captcha"
                             style="transform:scale(0.90);transform-origin:0;-webkit-transform:scale(0.90);transform:scale(0.90);-webkit-transform-origin:0 0;transform-origin:0 0; "></div>
                    </div>
                    <div class="row">
                        <!--<div class="col-md-6">
                           <a href="#password_recovery" id="forgot_from_3">need help?</a>
                       </div>-->
                        <div class="col-md-5 col-md-offset-7">
                            <input class="btn btn-theme04" type="submit" id="signup" name="signup" value="Sign Up">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </fieldset>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script src="assets/timezone/timezones.full.min.js"></script>
<script>
    $('select').timezones();
</script>
</body>
</html>